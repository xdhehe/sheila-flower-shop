<?php
//---Users VIEWS-------------------------------------
Route::get('/','PagesController@index');

Route::get('/about_us','PagesController@about');


Route::get('/valentines','PagesController@valentines');

	Route::get('/valentines/valentines-speical-1','PagesController@vd1');
	Route::get('/valentines/valentines-speical-2','PagesController@vd2');
	Route::get('/valentines/valentines-speical-3','PagesController@vd3');
	Route::get('/valentines/valentines-speical-4','PagesController@vd4');


Route::get('/roses','PagesController@roses');
	Route::get('/roses/romance','PagesController@romance');

		Route::get('/roses/romance/red','PagesController@romanceRed');
		Route::get('/roses/romance/pink','PagesController@romancePink');
		Route::get('/roses/romance/yellow','PagesController@romanceYellow');
		Route::get('/roses/romance/white','PagesController@romanceWhite');
		Route::get('/roses/romance/peach','PagesController@romancePeach');
		Route::get('/roses/romance/orange','PagesController@romanceOrange');
		Route::get('/roses/romance/perwinkle','PagesController@romancePerwinkle');
		Route::get('/roses/romance/simone','PagesController@romanceSimone');


	Route::get('/roses/passion','PagesController@passion');

		Route::get('/roses/passion/red','PagesController@passionRed');
		Route::get('/roses/passion/pink','PagesController@passionPink');
		Route::get('/roses/passion/yellow','PagesController@passionYellow');
		Route::get('/roses/passion/white','PagesController@passionWhite');
		Route::get('/roses/passion/peach','PagesController@passionPeach');
		Route::get('/roses/passion/orange','PagesController@passionOrange');
		Route::get('/roses/passion/perwinkle','PagesController@passionPerwinkle');
		Route::get('/roses/passion/simone','PagesController@passionSimone');

	Route::get('/roses/luxe','PagesController@luxe');

		Route::get('/roses/luxe/red','PagesController@luxeRed');
		Route::get('/roses/luxe/pink','PagesController@luxePink');
		Route::get('/roses/luxe/yellow','PagesController@luxeYellow');
		Route::get('/roses/luxe/white','PagesController@luxeWhite');

		Route::get('/roses/luxe/peach','PagesController@luxePeach');
		Route::get('/roses/luxe/orange','PagesController@luxeBlaze');
		Route::get('/roses/luxe/coral','PagesController@luxeCoral');
		Route::get('/roses/luxe/amber','PagesController@luxeAmber');

		Route::get('/roses/luxe/sparkle','PagesController@luxeSparkle');
		Route::get('/roses/luxe/twilight','PagesController@luxeTwilight');
		Route::get('/roses/luxe/twinkle','PagesController@luxeTwinkle');
		Route::get('/roses/luxe/shimmer','PagesController@luxeShimmer');



	Route::get('/roses/supreme','PagesController@supreme');

		Route::get('/roses/supreme/red','PagesController@supremeRed');
		Route::get('/roses/supreme/pink','PagesController@supremePink');
		Route::get('/roses/supreme/yellow','PagesController@supremeYellow');
		Route::get('/roses/supreme/white','PagesController@supremeWhite');


Route::get('/tulips','PagesController@tulips');
Route::get('/sunflower','PagesController@sunflower');
Route::get('/mixed','PagesController@mixed');

Route::get('/spring','PagesController@spring');
	Route::get('/spring/carnival','PagesController@springCarnival');
	Route::get('/spring/medley','PagesController@springMedley');
	Route::get('/spring/celebration','PagesController@springCelebration');
	Route::get('/spring/rhapsody','PagesController@springRhapsody');
	Route::get('/spring/pretty','PagesController@springPretty');
	Route::get('/spring/daffodil','PagesController@springDaffodil');

// Route::get('/circle','PagesController@circle');
Route::get('/complete','PagesController@complete');

// Route::get('/mothers-day','PagesController@mother');
// 	Route::get('/mothers-day/basket','PagesController@basket');
// 	Route::get('/mothers-day/pastel','PagesController@pastel');

// Route::get('/flowers','PagesController@flower');
// 	Route::get('/flowers/{category}','PagesController@deeper');
// 		Route::get('/flowers/{category}/{id}/order','PagesController@order');

Route::get('/services','PagesController@service');
	Route::get('/services/{name}','PagesController@inquire');

Route::get('/contact_us','PagesController@contact');

Route::get('/faq','PagesController@faq');


Route::post('/search','PagesController@search');
Route::get('/search/show/{id}','PagesController@show');

// Route::get('/inquiry_form','PagesController@inquiry_form');

//---Admin VIEWS-------------------------------------
Auth::routes();
Route::get('/dashboard', 'DashboardController@index');

Route::get('/dashboard/backup','DashboardController@backup');


Route::resource('/dashboard/categories/{categoryId}/flowers','FlowersController');
Route::put('updates','FlowersController@updates');
Route::delete('destroys','FlowersController@destroys');

Route::get('/dashboard/flowers/{id}/photo','FlowersController@photo');
	Route::post('photo_store','FlowersController@photo_store');
	Route::delete('photo_destroy','FlowersController@photo_delete');

Route::resource('/dashboard/services','ServicesController');
	Route::get('/dashboard/services/photos/{id}/index','ServicesController@photoIndex');
	Route::get('/dashboard/services/photos/{id}/create','ServicesController@photoAdd');
		Route::post('photostore','ServicesController@photoStore');
	Route::get('/dashboard/services/photos/{id}/edit/{photoId}','ServicesController@photoEdit');
		Route::put('photoupdate','ServicesController@photoUpdate');
	Route::delete('photoDestroy','ServicesController@photoDestroy');


Route::get('/dashboard/features/{category}','FeaturesController@index');
Route::get('/dashboard/features/{category}/create','FeaturesController@create');
	Route::post('store','FeaturesController@store');
Route::get('/dashboard/features/{category}/{id}/edit','FeaturesController@edit');
	Route::put('update','FeaturesController@update');
Route::delete('destroy','FeaturesController@destroy');

Route::resource('dashboard/categories','CategoriesController');


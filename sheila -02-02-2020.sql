-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 02, 2020 at 03:55 AM
-- Server version: 5.7.29-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sheila`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Arm Bouquets', '2018-12-20 00:01:53', '2018-12-20 00:01:53'),
(2, 'Hand-tied Bouquets', '2018-12-20 00:02:08', '2018-12-20 00:02:08');

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

CREATE TABLE `features` (
  `id` int(10) UNSIGNED NOT NULL,
  `order` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `features`
--

INSERT INTO `features` (`id`, `order`, `name`, `created_at`, `updated_at`, `category`) VALUES
(18, 2, '1754377305.jpg', '2018-12-27 18:16:58', '2018-12-27 18:16:58', 'featured'),
(21, 4, '800260024.jpg', '2019-01-12 07:10:24', '2019-01-12 07:10:24', 'featured'),
(22, 3, '1546488064.jpg', '2019-03-04 16:11:40', '2019-03-04 16:13:31', 'featured'),
(23, 1, '2015831254.jpg', '2019-03-04 16:18:51', '2019-03-06 17:12:27', 'featured');

-- --------------------------------------------------------

--
-- Table structure for table `flowers`
--

CREATE TABLE `flowers` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `picture` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `flowers`
--

INSERT INTO `flowers` (`id`, `category_id`, `order`, `name`, `description`, `price`, `created_at`, `updated_at`, `picture`) VALUES
(1, 1, 1, 'Red Romance', 'Red roses symbolize love and expressing your feelings to another person. It s color is also associated with passions, courage and respect.\r\n\r\nEvery bouquet is packaged in a classy water-proof wrapper with a matching ribbon. Each stem is kept fresh with water stored in a floral tube or floral foam. It comes with a greeting card where you can put into words how you feel.', NULL, '2018-12-28 22:22:22', '2018-12-28 22:30:54', '168342584.jpg'),
(2, 1, 2, 'Yellow Romance', 'Yellow roses are perfect gifts for friends to convey how much you appreciate their friendship. It shows care, delight and also symbolizes new beginnings.\r\n\r\nEvery bouquet is packaged in a classy water-proof wrapper with a matching ribbon. Each stem is kept fresh with water stored in a floral tube or floral foam. It comes with a greeting card where you can put into words how you feel.', NULL, '2018-12-28 22:39:06', '2018-12-28 22:39:06', '418500269.jpg'),
(3, 1, 3, 'Peach Romance', 'Peach roses speak for desire, gratitude and appreciation. This delicate color also symbolizes modesty, sincerity and get together with family and friends. \r\n\r\nEvery bouquet is packaged in a classy water-proof wrapper with a matching ribbon. Each stem is kept fresh with water stored in a floral tube or floral foam. It comes with a greeting card where you can put into words how you feel.', NULL, '2018-12-28 22:40:53', '2018-12-28 22:40:53', '1276654962.jpg'),
(4, 1, 4, 'Periwinkle Blue', 'Blue Ecuadorian Roses symbolizes something that is impossible to achieve like a hope for love. This unique floral color signifies also for miracles and mystery.\r\n\r\nEvery bouquet is packaged in a classy water-proof wrapper with a matching ribbon. Each stem is kept fresh with water stored in a floral tube or floral foam. It comes with a greeting card where you can put into words how you feel.', NULL, '2018-12-28 22:42:45', '2018-12-28 22:42:45', '255787328.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_09_20_132617_create_flowers_table', 1),
(4, '2018_10_15_130411_add_order', 2),
(8, '2018_10_15_230535_announcement', 3),
(9, '2018_10_17_021531_create_posts_table', 3),
(10, '2018_11_03_073948_create_pictures_table', 4),
(11, '2018_11_03_130914_added_foreignkey_to_pictures', 4),
(12, '2018_11_04_022845_create_services_table', 4),
(13, '2018_11_04_032121_drop_fpictures_then_add_it', 5),
(14, '2018_11_04_032320_add_order_in_service', 6),
(15, '2018_11_06_072308_create_features_table', 7),
(16, '2018_11_08_064025_add_column_category_to_features', 7),
(17, '2018_11_10_093532_create_categories_table', 8),
(18, '2018_11_12_083006_create_spictures_table', 9),
(20, '2018_11_14_021806_fpictures', 10),
(21, '2018_11_17_045046_drop_table_of_flowers_and_create_again', 11),
(22, '2018_11_17_063816_add_col_picture_in_flowers', 12);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('duke@gmail.com', '$2y$10$2U7kG21A9o.xcxafckhC4e5O81OIgVsRAGkPuJ7/tNUK64htHmBfi', '2018-10-19 02:15:45'),
('dp.boredstudent@gmail.com', '$2y$10$hrXIJ6Xhkg7QLp1/SuDCp.P2IZLY8.TrmPv88zGRjGpNlEAMXBwlq', '2018-11-20 19:26:35');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `title`, `description`, `created_at`, `updated_at`, `order`) VALUES
(1, 'Wedding Styling', 'All of us have our own dream weddings.  It does not matter if you’re a boy or a girl -  we all want this day to be unique and special!  Sheila May’s Blossoms will help turn your dream into reality by guiding you with distinctive style options to fulfill your wedding vision.  From church to the reception and beyond, from the first step in your grand entrance, through stunning floral drops amidst elegant scenery and warm ambience created especially for you, to minute details making your wedding even more unique, Sheila May’s Blossoms will harness its vast expertise and network to identify and fit your exact needs.  \r\n\r\nAt Sheila May’s Blossoms, we ensure that your wedding day is an exquisite experience that will frame new beginnings as you step into your married life’s exciting journey. \r\n\r\nWant to know more?  Book a no-cost coffee chat with us (coffee on us!) at sheilamayblossoms@icloud.com.', '2018-12-20 00:02:32', '2019-01-18 08:25:08', 1),
(2, 'Bride Entourage', 'We know, we know!  The entire wedding entourage is as excited as you in planning your special day.  Sheila May’s Blossoms will harness this thrilling energy into a creative process to fashion harmony for your wedding ensemble and craft a floral array only unique to you!   From the elegant bridal bouquet of meticulously hand-picked, hand-tied flowers, to equally delicate floral bouquets, wrist corsages, boutonnieres, head dresses and flower baskets, we will make sure that the sight and scent of your floral design will allow the entourage and guests to share the emotional experience of your wedding.\r\n\r\nWant to know more?  Book a no-cost coffee chat with us (coffee on us!) at sheilamayblossoms@icloud.com.', '2018-12-20 00:03:07', '2019-01-18 08:26:04', 2),
(3, 'Anniversaries & Birthdays', 'Celebrate your anniversaries and birthdays with attractive floral arrangements and backdrops to create an ambiance that is sure to be a long-lasting memory. \r\n\r\nPlease send us an email for a complimentary consultation at sheilamayblossoms@icloud.com.', '2018-12-20 00:04:13', '2019-01-05 06:48:21', 3),
(4, 'Corporate Events', 'Do you have a new restaurant to open or a company product launch? Make a positive impression on the first day of business by smartening up your place with our professionally styled flower designs. Allow us to create an ambiance of success on your inaugural ceremony.\r\n\r\nPlease send us an email for a complimentary consultation at sheilamayblossoms@icloud.com.', '2018-12-20 00:04:29', '2019-01-05 06:48:07', 4),
(5, 'Sympathy', 'Sending flowers is a tender, heartfelt expression of sympathy, comfort and respect.\r\n\r\nPlease send us an email for a complimentary consultation at sheilamayblossoms@icloud.com.', '2018-12-20 00:04:40', '2019-01-05 06:47:50', 5);

-- --------------------------------------------------------

--
-- Table structure for table `spictures`
--

CREATE TABLE `spictures` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `spictures`
--

INSERT INTO `spictures` (`id`, `service_id`, `name`, `order`, `created_at`, `updated_at`) VALUES
(3, 3, '160874560.jpg', 1, '2019-01-05 06:46:48', '2019-01-05 06:46:48'),
(6, 1, '2028367095.jpg', 2, '2019-01-18 08:19:50', '2019-01-18 08:40:18'),
(8, 1, '792749044.jpg', 3, '2019-01-18 08:20:26', '2019-01-18 08:20:26'),
(9, 2, '996794304.jpg', 1, '2019-01-18 08:26:21', '2019-01-18 08:26:21'),
(10, 2, '1405872825.jpg', 2, '2019-01-18 08:26:30', '2019-01-18 08:26:30'),
(11, 2, '1738438492.jpg', 3, '2019-01-18 08:26:36', '2019-01-18 08:26:36'),
(12, 2, '839838277.jpg', 4, '2019-01-18 08:26:42', '2019-01-18 08:26:42'),
(13, 1, '43317691.jpg', 1, '2019-01-18 08:39:54', '2019-01-18 08:40:08'),
(14, 1, '263033738.jpg', 4, '2019-01-18 08:41:06', '2019-01-18 08:41:06'),
(15, 1, '616574784.jpg', 5, '2019-01-18 08:41:11', '2019-01-18 08:41:11'),
(16, 4, '153435129.jpg', 1, '2019-01-18 08:43:33', '2019-01-18 08:43:33'),
(17, 4, '2135563022.jpg', 2, '2019-01-18 08:43:39', '2019-01-18 08:43:39'),
(18, 4, '1249560958.jpg', 3, '2019-01-18 08:43:44', '2019-01-18 08:43:44'),
(19, 4, '1311666865.jpg', 4, '2019-01-18 08:43:49', '2019-01-18 08:43:49'),
(20, 4, '1449995860.jpg', 5, '2019-01-18 08:43:55', '2019-01-18 08:43:55'),
(21, 4, '1487091057.jpg', 6, '2019-01-18 08:44:00', '2019-01-18 08:44:00'),
(33, 5, '1246743222.jpg', 1, '2020-01-18 17:16:23', '2020-01-18 17:16:23'),
(34, 5, '1470425542.jpg', 2, '2020-01-18 17:16:46', '2020-01-18 17:16:46'),
(35, 5, '636586703.jpg', 3, '2020-01-18 17:16:52', '2020-01-18 17:16:52'),
(36, 5, '768970068.jpg', 4, '2020-01-18 17:16:57', '2020-01-18 17:16:57'),
(37, 5, '1340645810.jpg', 5, '2020-01-18 17:17:04', '2020-01-18 17:17:04'),
(38, 5, '660083611.jpg', 6, '2020-01-18 17:17:12', '2020-01-18 17:17:12'),
(39, 5, '138877657.jpg', 7, '2020-01-18 17:17:21', '2020-01-18 17:17:21'),
(40, 5, '1583759639.jpg', 8, '2020-01-18 17:17:30', '2020-01-18 17:17:30'),
(41, 5, '1269421332.jpg', 9, '2020-01-18 17:17:35', '2020-01-18 17:17:35'),
(42, 5, '769858404.jpg', 10, '2020-01-18 17:17:41', '2020-01-18 17:17:41'),
(43, 5, '1659050074.jpg', 11, '2020-01-18 17:17:49', '2020-01-18 17:17:49'),
(44, 5, '1340160617.jpg', 12, '2020-01-18 17:17:58', '2020-01-18 17:17:58');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(4, 'Duke Punzalan', 'duke@gmail.com', NULL, '$2y$10$P4xbDSdQu5i4S8fhWsSoIeuhptAs4mNhtEqpEybUkqFWv1zrEyikq', NULL, '2020-01-18 16:49:07', '2020-01-18 16:49:07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `features`
--
ALTER TABLE `features`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flowers`
--
ALTER TABLE `flowers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `spictures`
--
ALTER TABLE `spictures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `features`
--
ALTER TABLE `features`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `flowers`
--
ALTER TABLE `flowers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `spictures`
--
ALTER TABLE `spictures`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

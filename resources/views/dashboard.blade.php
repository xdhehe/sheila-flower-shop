@extends('layouts.app')
@section('title','Sheila Flowershop Dashboard')
@section('content')

<div class="container mt-5">
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    <div class="card-header pt-4"><center><h4><i class="fas fa-chart-line"></i> Dashboard</h4></center></div>

    <div class="justify-content-center">
        <div class="row">
            <div class="col-md-6 mt-5">
                <div class="card">
                    <a href="{{url('/dashboard/features/featured')}}"><li class="btn btn-warning list-group-item text-dark pt-3 mt-5"><h5>Show/ Modify Featured Photos(s)</h5></li></a>
                </div>
            </div><!-- end of col -->

            <div class="col-md-6 mt-5">
                <div class="card">
                    <a href="{{url('/dashboard/categories')}}"> <li class="btn btn-warning list-group-item text-dark pt-3"><h5>Show/ Modify Flower(s)</h5></li></a>
                    <a href="{{url('/dashboard/services')}}"><li class="btn btn-warning list-group-item text-dark pt-3 mt-4"><h5>Show/ Modify Services(s)</h5></li></a>
                </div>
            </div><!-- end of col -->
        </div><!-- end of row -->
    </div>
</div>
@endsection
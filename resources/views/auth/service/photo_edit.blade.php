@extends('layouts.app')
@section('title','Sheila Flowershop Service(s) Add Photo')
@section('content')
@include('sections.messeges')

<form method="post" action="{{ action('ServicesController@photoUpdate')}}">
@csrf	
	<h2>Edit Photo</h2>
	<input type="hidden" name="id" value="{{$service->id}}">
	<input type="hidden" name="pictureId" value="{{$spicture->id}}">

	<div class="row">
		<div class="input-group w-50">
		  	<div class="input-group-prepend">
	    		<h4 class="pt-2 mr-3">Order:</h4>
			</div>
	    	<input type="text" class="form-control" name="order" value="{{$spicture->order}}">
		</div>

		<div class="col-md-6 col-sm-12 m-4 text-center">
		    <?php $path = "images/services/$service->id/$spicture->name"?>
		    <img src="{{asset($path)}}" class="w-100 h-50 mb-3">

		    	<?php $path = "/dashboard/services/photos/$service->id/index"?>
			    <a href="{{url($path)}}"> {{Form::button('Back',['class' =>'btn btn-secondary'])}}</a>
				{{Form::submit('Update',['class'=>'btn btn-success'])}}
		</div>
	</div><!-- end of row -->
	
{{FORM::hidden('_method','PUT')}}
</form>

@endsection
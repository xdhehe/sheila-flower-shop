@extends('layouts.app')
@section('title','Sheila Flowershop Service(s)')
@section('content')
	<h1>{{count($services) > 1 ? "Services" : "Service" }}</h1>
	<div class="mt-1 mb-4">
		<a href="{{url('/dashboard')}}">
			<button class="btn btn-secondary">Back</button>
		</a>
		<a href="{{url('/dashboard/services/create') }}"><button class="btn btn-success">Create Service</button></a>
	</div>
		@include('sections.messeges')
		@if(count($services) > 0)

			@foreach($services as $service)
				<div class="container row">
					<div class="col-sm-12 col-md-11 card p-3">
						<h5>
							<b>Name:</b><i> {{$service->title}}</i>
							<div class="float-right"><b>Order:</b> <i>{{$service->order}}</i></div><br>
						</h5>
							<b>Description:</b> {{$service->description}}
					</div>

					<div class="row col-sm-12 col-md-1">
						<a href="/dashboard/services/photos/{{$service->id}}/index"><button class="btn btn-dark  ml-2">PHOTOS</button></a>
						<a href="/dashboard/services/{{$service->id}}/edit"><button class="btn btn-primary  ml-2 pl-4 pr-4">EDIT</button></a>
						<form action="{{route('services.destroy',$service->id)}}" method="post">
						@csrf
							<button class="btn btn-danger ml-2">DELETE</button>
						</form>
					</div>
				</div>
				<br>
			@endforeach
			<!-- for pagination -->
		@else
			<h3 class="ml-2">No Service yet :( </h3>
		@endif
@endsection

@extends('layouts.app')
@section('title','Sheila Flowershop Service(s)')
@section('content')
@include('sections.messeges')
	<h2>Create Service</h2>
	<div class="m-4">
		{!! Form::open(['action' => 'ServicesController@store','method' => 'POST']) !!}
		@csrf
		    <div class="row">
			    <div class="col-md-2 col-sm-12">
				    <h4>Order:</h4>
			    </div>
				<div class="col-md-5 col-sm-12">
				    <input type="number" class="form-control" name="order" min="1" value="{{$last}}" required><br>
				</div>			    
			</div>		    

		    <div class="row">
			    <div class="col-md-2 col-sm-12">
				    <h4>Name:</h4>
			    </div>
				<div class="col-md-5 col-sm-12">
				    <input type="text" class="form-control" name="name" required><br>
				</div>			    
			</div>

		    <div class="row">
			    <div class="col-md-2 col-sm-12">
				    <h4>Description:</h4>
			    </div>
				<div class="col-md-5 col-sm-12">
					<textarea class="form-control" rows="5" name="description" required></textarea><br>
				</div>			    
			</div>			
			<a href="{{url('/dashboard/services')}}"> 
				{{Form::button('Back',['class' =>'btn btn-secondary'])}}
			</a>
			{{Form::submit('Submit',['class'=>'btn btn-success'])}}
		{!! Form::close() !!}
	</div>
@endsection
@extends('layouts.app')
@section('title','Sheila Flowershop Service(s)')
@section('content')
@include('sections.messeges')
	<h2>Edit Service</h2>
	<div class="m-4">
		<form method="post" action="{{ action('ServicesController@update',$service->id)}}">
		@csrf
		    <div class="row">
			    <div class="col-md-2 col-sm-12">
				    <h4>Order:</h4>
			    </div>
				<div class="col-md-5 col-sm-12">
				    <input type="number" class="form-control" name="order" min="1" value="{{$service->order}}" required><br>
				</div>			    
			</div>		    

		    <div class="row">
			    <div class="col-md-2 col-sm-12">
				    <h4>Name:</h4>
			    </div>
				<div class="col-md-5 col-sm-12">
				    <input type="text" class="form-control" name="name" value="{{$service->title}}" required><br>
				</div>			    
			</div>

		    <div class="row">
			    <div class="col-md-2 col-sm-12">
				    <h4>Description:</h4>
			    </div>
				<div class="col-md-5 col-sm-12">
					<textarea class="form-control" rows="5" name="description" required>{{$service->description}}</textarea><br>
				</div>			    
			</div>			
			<a href="{{url('/dashboard/services')}}"> 
				{{Form::button('Back',['class' =>'btn btn-secondary'])}}
			</a>
			{{Form::submit('Update',['class'=>'btn btn-success'])}}
		{{FORM::hidden('_method','PUT')}}
		</form>
	</div>
@endsection
@extends('layouts.app')
@section('title','Sheila Flowershop Service(s) Photo(s)')
@section('content')
@include('sections.messeges')

	<h2>{{$service->title}} Photos</h2>

	<a href="{{url('/dashboard/services')}}">
		<button class="btn btn-secondary pl-4 pr-4">Back</button>
	</a>

	<?php $pathCreate = "/dashboard/services/photos/$id/create" ?>
	<a href="{{$pathCreate}}">
		<input type="button" name="upload" class="btn btn-success pl-4 pr-4" value="Add Photo">
	</a>
<hr>

@if(count($photos) > 0)
	<div class="row text-center">
		@foreach($photos as $photo)
			<div class="col-md-4 col-sm-12">
				<?php $path = "images/services/$id/$photo->name";?>
				<h3>Order: {{$photo->order}}</h3>
				<img src="{{asset($path)}}" class="p-3" style="height: 300px">

				<?php $pathEdit = "/dashboard/services/photos/$id/edit/$photo->id";?>
				<a href="{{$pathEdit}}">
					<button class="btn btn-primary  ml-2 pl-4 pr-4">EDIT</button>
				</a>
				<form method="post" action="{{ action('ServicesController@photoDestroy')}}">
				@csrf

					<input type="hidden" name="id" value="{{$id}}">
					<input type="hidden" name="name" value="{{$photo->name}}">
					<input type="hidden" name="photoId" value="{{$photo->id}}">

					<button class="btn btn-danger pl-4 pr-4">Delete</button>

				<input type="hidden" name="_method" value="delete" />
				</form>

				<hr class="mt-3">
			</div>
		@endforeach
	</div><!-- end of row -->
@else
	<h3 class="ml-2">No photos yet :( </h3>
@endif

@endsection

@extends('layouts.app')
@section('title','Sheila Flowershop Service(s) Add Photo')
@section('content')
@include('sections.messeges')

<h2>{{$service->title}} Add Photo</h2>

<form method="post" action="{{ action('ServicesController@photoStore') }}" enctype="multipart/form-data">
@csrf
<input type="hidden" name="service_id" value="{{$service->id}}">

	<div class="row mt-4">
		<div class="col-md-3 col-sm-4">
			<h4 class="pt-2">Order: </h4><br>
		</div>

		<div class="col-md-4 col-sm-8">
			<input type="number" name="order" class="form-control" min="1" value="{{$photosCount}}">
		</div>
	</div><!-- end of row -->

	<div class="row">
		<div class="col-md-3 col-sm-4">
			<h4 class="pt-2">Select File for Upload: </h4><br>
		</div><!-- end of col -->

		<div class="col-md-4 col-sm-8 mt-2">
		    <input type="file" name="image" value="Choose File" required>
		</div><!-- end of col -->
	</div> <!-- end of row -->

	<p class="text-hide">{{$path = "/dashboard/services/photos/$service->id/index"}}</p>
	<a href="{{url($path)}}" class="btn btn-secondary">Back</a>

	<input type="submit" name="upload" class="btn btn-success" value="Submit"><br>
	<h5 class="mt-2">Note: Image with file extensions with jpg, png, jpeg are allowed</h5>
</form>


@endsection

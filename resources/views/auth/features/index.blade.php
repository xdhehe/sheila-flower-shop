@extends('layouts.app')
@section('title','Sheila Flowershop Feature(s)')
@section('content')
@include('sections.messeges')
	<p class="text-hide">{{$pathCreate = "/dashboard/features/$category/create"}}</p>

	<h1>Featured Photo(s)</h1>

	<a href="{{url('/dashboard')}}">
		<button class="btn btn-secondary pl-4 pr-4">Back</button>
	</a>
	<a href="{{$pathCreate}}">
		<input type="button" name="upload" class="btn btn-success pl-4 pr-4" value="Create">
	</a>
<hr>
@if(count($photos) > 0)
	<div class="row text-center">
		@foreach($photos as $photo)
			<div class="col-md-4 col-sm-12">
				<p class="text-hide">{{$path = "images/features/$photo->name"}}</p>
				<h3>Order: {{$photo->order}}</h3>
				<img src="{{asset($path)}}" class="p-5" style="height: 250px">

				<form method="post" action="{{ action('FeaturesController@destroy')}}">
				@csrf
					<p class="text-hide">{{$pathEdit = "/dashboard/features/$category/$photo->id/edit"}}</p>
					<a href="{{$pathEdit}}">
						{{Form::button('EDIT',['class'=>'btn btn-primary  ml-2 pl-4 pr-4'])}}
					</a>

					<button class="btn btn-danger pl-4 pr-4">Delete</button>
					<input type="hidden" name="id" value="{{$photo->id}}">
					<input type="hidden" name="name" value="{{$photo->name}}">
					<input type="hidden" name="category" value="{{$category}}">

				<input type="hidden" name="_method" value="delete" />
				</form>

				<hr class="mt-3">
			</div>
		@endforeach
	</div><!-- end of row -->
@else
	<h3 class="ml-2">No photos yet :( </h3>
@endif

@endsection

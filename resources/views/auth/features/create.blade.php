@extends('layouts.app')
@section('title','Sheila Flowershop Feature(s) Create')
@section('content')
@include('sections.messeges')

<form method="post" action="{{ action('FeaturesController@store') }}" enctype="multipart/form-data">
@csrf
	<div class="row mt-4">
		<div class="col-md-3 col-sm-4">
			<h4 class="pt-2">Order: </h4><br>
		</div>

		<div class="col-md-4 col-sm-8">
			<input type="hidden" name="category" value="{{$category}}">
			<input type="number" name="order" class="form-control" min="1" value="{{$photos}}">
		</div>
	</div><!-- end of row -->

	<div class="row">
		<div class="col-md-3 col-sm-4">
			<h4 class="pt-2">Select File for Upload: </h4><br>
		</div><!-- end of col -->
		
		<div class="col-md-4 col-sm-8 mt-2">
			
		    <input type="file" name="image" value="Choose File" required>
		</div><!-- end of col -->
	</div> <!-- end of row -->

	<p class="text-hide">{{$path = "/dashboard/features/$category"}}</p>

	<a href="{{url($path)}}">
			 {{Form::button('Back',['class' =>'btn btn-secondary'])}}
	</a>
	<input type="submit" name="upload" class="btn btn-success" value="Submit"><br>
	<h5 class="mt-2">Note: Image with file extensions with jpg, png, jpeg are allowed</h5>
</form>
@endsection

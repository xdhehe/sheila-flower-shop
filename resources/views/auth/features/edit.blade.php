@extends('layouts.app')
@section('title','Sheila Flowershop Feature(s) Edit')
@section('content')
@include('sections.messeges')

<form method="post" action="{{ action('FeaturesController@update')}}">
@csrf	
	<h2>Edit Photo</h2>
	<input type="hidden" name="id" value="{{$id}}">
	<input type="hidden" name="category" value="{{$category}}">
	<div class="row">
		<div class="input-group w-50">
		  	<div class="input-group-prepend">
	    		<h4 class="pt-2 mr-3">Order:</h4>
			</div>
	    	<input type="text" class="form-control" name="order" value="{{$photo->order}}">
		</div>

		<div class="col-md-6 col-sm-12 m-4 text-center">
		    <p class="text-hide">{{$path = "images/features/$photo->name"}}</p>
		    <img src="{{asset($path)}}" class="w-100 h-75">

		    	<p class="text-hide">{{$path = "/dashboard/features/$category"}}</p>
			    <a href="{{url($path)}}"> {{Form::button('Back',['class' =>'btn btn-secondary'])}}</a>
				{{Form::submit('Update',['class'=>'btn btn-success'])}}
		</div>
	</div><!-- end of row -->
	
{{FORM::hidden('_method','PUT')}}
</form>
@endsection
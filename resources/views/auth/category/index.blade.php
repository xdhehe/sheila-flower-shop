@extends('layouts.app')
@section('title','Sheila Flowershop Categories')
@section('content')
@include('sections.messeges')
	<a href="{{url('/dashboard')}}">
		<h1 class="text-dark">
			<button class="btn btn-secondary">Back</button>
			{{count($categories) > 1 ? "Categories" : "Category" }}
		</h1>
	</a>
	<hr>
	{!! Form::open(['action' => 'CategoriesController@store','method' => 'POST']) !!}
	@csrf
	    <div class="row">
		    <div class="col-md-3 col-sm-12">
			    <h4>Create Category:</h4>
		    </div>
			<div class="col-md-5 col-sm-12">
			    <input type="text" class="form-control" name="name" required><br>
			</div>
			<div class="col-md-1 col-sm-12">
				{{Form::submit('Submit',['class'=>'btn btn-success'])}}
			</div>
		</div><hr>
	{!! Form::close() !!}

		@if(count($categories) > 0)
		<div class="row">
		@foreach($categories as $category)
			<div class="col-sm-12 col-md-4 mt-3 border border-warning">
				<br>
				<div class="input-group">
		            <div class="input-group-prepend">
            	<form  class="delete" action="{{action('CategoriesController@destroy' , $category->id)}}" method="post">
				@csrf
		              <input type="submit" class="btn btn-danger" value="Delete">
    			<input type="hidden" name="_method" value="delete">
				</form>
		            </div>
	            <form action="{{action('CategoriesController@update',$category->id)}}" method="post">
				@csrf
	            	<div class="input-group-append">
		            <input type="text" class="form-control" name="name" value="{{$category->name}}">
			    		<button class="btn btn-info text-white">Update</button>
			  		</div>
			  		<input type="hidden" name="_method" value="put">
			  	</form>
		        </div>
		        <?php $flowersPath = "/dashboard/categories/$category->id/flowers" ?>
		        <a href="{{url($flowersPath)}}">
					<button class="btn btn-primary w-100">Flowers</button>
				</a><br><br>
	        </div><!-- end of col -->

		@endforeach
		</div>

		@else
			<h3 class="ml-2">No categories yet :( </h3>
		@endif

@endsection

@section('js')
<script>
    $(".delete").on("submit", function(){
        return confirm("Warning! Are you sure you want to delete this Category?; Note: Related fields will be deleted too.");
    });
</script>
@endsection
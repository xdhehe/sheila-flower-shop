@extends('layouts.app')
@section('title','Sheila Flowershop Flower(s)')
@section('content')
@include('sections.messeges')

<h1>{{$category->name}}</h1>
<div class="mt-1 mb-4">
	<a href="{{url('/dashboard/categories')}}">
		<button class="btn btn-secondary">Back</button>
	</a>
	<?php $pathCreate = "/dashboard/categories/$category->id/flowers/create" ?>
	<a href="{{url($pathCreate)}}">
		<button class="btn btn-success">Create Flower</button>
	</a><hr>
</div>
	@if(count($flowers) > 0)

	@foreach($flowers as $flower)
	<input type="hidden" name="category" value="{{$flower->category_id}}">
	<div class="row">
		<div class="col-sm-12 col-md-9">
				<form action="{{action('FlowersController@updates')}}" method="post">
				@csrf
				<input type="hidden" name="id" value="{{$flower->id}}">
				<input type="hidden" name="category" value="{{$flower->category_id}}">
			<div class="row">
				<div class="col-md-2 pt-2">
					<h4>Name: </h4>
				</div>
				<div class="col-md-5">
					<input type="text" class="form-control" name="name" value="{{$flower->name}}">	
				</div>

				<div class="col-md-2 pt-2">
					<h4>Order: </h4>
				</div>
				<div class="col-md-3">
					<input type="text" class="form-control" name="order" value="{{$flower->order}}">	
				</div>	

				<div class="col-md-2 pt-2">
					<h4>Description: </h4>
				</div>
				<div class="col-md-5">
					<textarea class="form-control" name="description" rows="5">{{$flower->description}}</textarea>
				</div>
				<div class="col-md-2 pt-2">
					<h4>price: </h4>
				</div>
				<div class="col-md-3">
		            <input type="text" class="form-control" name="price" value="{{$flower->price}}">
					<br>
	    			<button class="btn btn-info text-white w-100 pt-2 pb-2">Update</button>
		  		<input type="hidden" name="_method" value="put">
			  	</form>
			  	
				  	<form  class="delete" action="{{action('FlowersController@destroys')}}" method="post">
					<input type="hidden" name="category" value="{{$flower->category_id}}">
			  		<input type="hidden" name="id" value="{{$flower->id}}">
			  		<input type="hidden" name="picture" value="{{$flower->picture}}">
						@csrf
		                <input type="submit" class="btn btn-danger w-100 pt-2 pb-2" value="Delete">
						<input type="hidden" name="_method" value="delete">
					</form>
				</div><!-- end of col -->
			</div><!-- end of row insied of row -->
		</div><!-- end of col -->

		<div class="col-sm-12 col-md-3 mt-3">
			<?php $pathImage = "images/Flowers/$flower->category_id/$flower->picture"?>
			<center>
				<img src="{{asset($pathImage)}}" class="h-25">
			</center>
		</div>
	</div>

	<hr>
	@endforeach

	@else
		<h3 class="ml-2">No Flowers...</h3>
	@endif
@endsection
@section('js')
<script>
    $(".delete").on("submit", function(){
        return confirm("Warning! Are you sure you want to delete this Flower?");
    });
</script>
@endsection
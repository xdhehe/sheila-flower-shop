@extends('layouts.app')
@section('title','Sheila Flowershop Flower Create')
@section('content')
	<h2 class="mt-2">Create Flower for {{$localCategory->name}}</h2>
	<div class="m-4">
		@include('sections.messeges')
		<form method="post" action="{{action('FlowersController@store', $categoryId)}}"  enctype="multipart/form-data">
		@csrf

			<input type="hidden" name="category" value="{{$categoryId}}">

		    <div class="row">
			    <div class="col-md-2 col-sm-12">
				    <h4>Order:</h4>
			    </div>
				<div class="col-md-5 col-sm-12">
				    <input type="number" class="form-control" name="order" min="1" value="{{$count}}" required><br>
				</div>			    
			</div><!-- end of row order -->

		    <div class="row">
			    <div class="col-md-2 col-sm-12">
				    <h4>Name:</h4>
			    </div>
				<div class="col-md-5 col-sm-12">
				    <input type="text" class="form-control" name="name" required><br>
				</div>			    
			</div>

		    <div class="row">
			    <div class="col-md-2 col-sm-12">
				    <h4>Description:</h4>
			    </div>
				<div class="col-md-5 col-sm-12">
					<textarea class="form-control" rows="5" name="description"></textarea><br>
				</div>			    
			</div>				

		    <div class="row">
			    <div class="col-md-2 col-sm-12">
				    <h4>Price:</h4>
			    </div>
				<div class="col-md-5 col-sm-12">
				    <input type="text" class="form-control" name="price" value=""><br>
				</div>			    
			</div><!-- end of row order -->						

		    <div class="row">
			    <div class="col-md-2 col-sm-12">
				    <h4>Picture:</h4>
			    </div>
				<div class="col-md-5 col-sm-12">
				    <input type="file" name="image" value="Choose File" required>
				    <h5 class="mt-2">Note: Image with file extensions with jpg, png, jpeg are allowed</h5>
				</div>			    
			</div><!-- end of row order -->									

			<?php $pathBack = "/dashboard/categories/$categoryId/flowers"?>
			<a href="{{url($pathBack)}}">
			 {{Form::button('Back',['class' =>'btn btn-secondary'])}}
			</a>
			{{Form::submit('Submit',['class'=>'btn btn-success'])}}
		</form>
	</div>
@endsection
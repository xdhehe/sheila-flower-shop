@extends('layouts.app')
@section('title','Sheila Flowershop Announcement Create')
@section('content')
	<h2>Create Announcement</h2>
	<div class="m-4">
		@include('sections.messeges')
		{!! Form::open(['action' => 'PostsController@store','method' => 'POST']) !!}
		@csrf
			<div class="row">
				<div class="col-md-4 col-sm-12 mb-2">
					<h4>Date</h4>
				    <input type="date" class="form-control" name="date" required>
			    </div>
			</div><!-- END OF ROW -->

			<div class="row">
			    <div class="col-md-6 col-sm-12">
				    <h4>Your Announcement</h4>
				  	<textarea class="form-control" rows="5" name="description" required></textarea><br>
					<a href="{{url('/dashboard/posts')}}">	
					  {{Form::button('Back',['class' =>'btn btn-secondary'])}}
				 	</a>
					{{Form::submit('Submit',['class'=>'btn btn-success'])}}
				</div>
			</div>

		{!! Form::close() !!}
	</div>
@endsection
@extends('layouts.app')
@section('title','Sheila Flowershop Create Announcement Edit')
@section('content')
@include('sections.messeges')
	<h2>Edit Announcement</h2>
	<div class="m-4">
		{!! Form::open(['action' => ['PostsController@update', $post->id],'method' => 'POST']) !!}
		@csrf
			<div class="row">
				<div class="col-md-4 col-sm-12 mb-2">
					<h4>Date</h4>
				    <input type="date" class="form-control" name="date" value="{{$post->date}}" required>
			    </div>

			    <div class="col-md-12 col-sm-12">
				    <h4>Your Announcement</h4>
				  	<textarea class="form-control" rows="5" name="description" required>{{$post->description}}</textarea><br>
				  	<a href="{{url('/dashboard/posts')}}">
						{{Form::button('Back',['class' =>'btn btn-secondary'])}}
					</a>
					{{Form::submit('Submit',['class'=>'btn btn-success'])}}
				</div>
			</div><!-- END OF ROW -->

		{{FORM::hidden('_method','PUT')}}
		{!! Form::close() !!}
	</div>
@endsection
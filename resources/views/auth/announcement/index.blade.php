@extends('layouts.app')
@section('title','Sheila Flowershop Announcement(s)')
@section('content')
	<h1>{{count($posts) > 1 ? "Announcements" : "Announcement" }}</h1>

	<div class="mt-1 mb-4">
		<a href="{{url('/dashboard')}}">
			<button class="btn btn-secondary">Back</button>
		</a>
		<a href="{{url('/dashboard/posts/create') }}"><button class="btn btn-success">Create Post</button></a>
	</div>

		@include('sections.messeges')
		@if(count($posts) > 0)

			@foreach($posts as $post)
				<div class="container row">
					<div class="col-sm-12 col-md-11 card p-3">
						<h4><i>{{$post->date}}</i></h4>
				  		&nbsp;{{$post->description}}
					</div>

					<div class="row col-sm-12 col-md-1">
						<a href="/dashboard/posts/{{$post->id}}/edit">{{Form::button('EDIT',['class'=>'btn btn-primary  ml-2 pl-4 pr-4'])}}</a>

						{!! Form::open(['action' => ['PostsController@destroy', $post->id],'method' => 'POST']) !!}
						@csrf
						
							<button class="btn btn-danger ml-2">DELETE</button>
						
						{{FORM::hidden('_method','DELETE')}}
						{!! Form::close() !!}
					</div>
				</div>
				<br>
			@endforeach

		@else
			<h3 class="ml-2">No post yet :( </h3>
		@endif

@endsection
<style>
.icon { 
    width: 25;
}

.navbar{
  background: rgba(180, 46, 117, 0.6);
}

.head{
  font-family: Comic Sans MS;
  font-size: 20px;
}

.dropdown:hover>.dropdown-menu {
  display: block;
}

.dropdown>.dropdown-toggle:active {
    pointer-events: none;
}

/*body { 
  background: url('{{asset('images/general/bggg.jpg')}}') no-repeat center center fixed;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}*/

.dropdown-menu a:hover {background-color: #93C83A;}

* {cursor: url({{asset('images/general/cursorr.cur')}}), default; }

.footer {
 position:fixed;
    bottom:0;
}
</style>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<!--Logo of TAB  -->
<link rel="icon" type="image/gif/png" href="{{asset('images/general/logo_icon.png')}}">
<!-- BS CDN -->
<link rel="stylesheet" href="{{asset('css/app.css')}}">
<!-- FONT AWESOME -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous"> 
<!-- navbar to -->
<script type="text/javascript" src="{{asset('js/app.js')}}"></script>

  <title>@yield('title')</title>
</head>
  <body class="bg mb-5" oncontextmenu="return false;">
      @section('header')
      @show
      @include('sections.user_header')  
    <div class="container" >
      @yield('content')
    </div><!-- END OF CONTAINER -->
      @section('footer')
      @include('sections.user_footer')
  </body>
</html>

@section('js')
  @show
  <script>
//preventing users to right click
    window.onload = function() {
      document.addEventListener("contextmenu", function(e){
        e.preventDefault();
        alert("Disabling right click, protecting copyrighted content.");
      }, false);
    };
  </script>
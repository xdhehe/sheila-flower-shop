<style type="text/css">.icon { 
    width: 25;
}    

.navbar{
  background: rgba(180, 46, 117, 0.6);
}

.head{
  font-family: Comic Sans MS;
  font-size: 20px;
}

.dropdown:hover>.dropdown-menu {
  display: block;
}

.dropdown>.dropdown-toggle:active {
    pointer-events: none;
}
</style>

<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="description" content="SheilaMay's Blossoms Flower and Gift Shop">
  <meta name="keywords" content="Flower,Flowers,Rose,Roses,Tulip,Tulips,Buoquet,Buoquets,Sunflower,Valentines,Delivery">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
<!--Logo of TAB  -->
  <link rel="icon" type="image/gif/png" href="{{asset('images/general/logo_icon.png')}}">
<!-- BS CDN -->
  <link rel="stylesheet" href="{{asset('css/app.css')}}">
 <!-- FONT AWESOME -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">   
  
  <title>@yield('title')</title>

</head>
<body class="bg mb-5">

  @section('header')
  
  <nav class="navbar navbar-expand-md navbar-dark mb-2">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample08" aria-controls="navbarsExample08" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <a class="navbar-brand head" href="{{ url('/') }}">Sheila May's Flower Shop <img src="{{asset('images/general/logo_icon.png')}}"></a>

      <div class="collapse navbar-collapse justify-content-md-center" id="navbarsExample08">
        <ul class="navbar-nav ml-auto">

            @guest
                <li class="nav-item">
                    <a class="nav-link active" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                <li class="nav-item">
                    @if (Route::has('register'))
                        <a class="nav-link active" href="{{ route('register') }}">{{ __('Register') }}</a>
                    @endif
                </li>
            @else
                <li class="nav-item active dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{url('/dashboard')}}">Dashboard</a>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            @endguest        
        </ul>            
      </div><!-- END OF COLLAPSABLE -->
    </nav><!-- END OF WHOLE NAV -->

<div class="container" >
    @yield('content')
  

  @section('footer')
  <footer class="navbar text-white mt-5">
      <p>Copyright © 2018 sheilasflowershop.com, All Rights Reserved.</p>
      <p class="float-right"><i><a href="#">Back to top</a></i></p>
  </footer>

</div><!-- END OF CONTAINER -->


</body>
</html>
<script type="text/javascript" src="{{asset('js/app.js')}}"></script>          
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
@section('js')
@show
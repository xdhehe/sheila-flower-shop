@extends('layouts.user')
@section('title', 'Sheilablossoms Mixed')
@section('content')
<div class="mt-2">
	<small>
		<a href="{{url('/')}}" class="text-secondary">Home</a> /&nbsp;
		<a href="{{url('/')}}" class="text-secondary">Flowers</a> /&nbsp;
		Mixed Bouquets
	</small>
</div>
<div class="row">
	<div class="col-6 col-md-3 mt-5">
		<center>
			<div style="color:#b82171">
			<img data-enlargable style="cursor: zoom-in" src="{{asset('/images/flowers/mixed/scarlet.jpeg')}}" class="w-100" height="320px">
				<h5 class="mt-3">Scarlet</h5>
				<h6 class="text-secondary">(6 red roses, 1 pink stargazer, eucalyptus, misty)</h6>
				<h6 class="text-secondary">₱1,800</h6>
			</div>
		</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<center>
			<div style="color:#b82171">
			<img data-enlargable style="cursor: zoom-in" src="{{asset('/images/flowers/mixed/tiffany.jpeg')}}" class="w-100" height="320px">
				<h5 class="mt-3">Tiffany</h5>
				<h6 class="text-secondary">(6 pink roses, 1 pink stargazer, eucalyptus, misty)</h6>
				<h6 class="text-secondary">₱1,800</h6>
			</div>
		</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<center>
			<div style="color:#b82171">
			<img data-enlargable style="cursor: zoom-in" src="{{asset('/images/not.jpg')}}" class="w-100" height="320px">
				<h5 class="mt-3">Margarita</h5>
				<h6 class="text-secondary">(6 yellow roses, 1 yellowin, eucalyptus, misty)</h6>
				<h6 class="text-secondary">₱1,800</h6>
			</div>
		</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<center>
			<div style="color:#b82171">
			<img data-enlargable style="cursor: zoom-in" src="{{asset('/images/flowers/mixed/sangria.jpeg')}}" class="w-100" height="320px">
				<h5 class="mt-3">Sangria</h5>
				<h6 class="text-secondary">(12 red roses, 1 yellowin, eucalyptus, misty)</h6>
				<h6 class="text-secondary">₱2,400</h6>
			</div>
		</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<center>
			<div style="color:#b82171">
			<img data-enlargable style="cursor: zoom-in" src="{{asset('/images/flowers/mixed/flamingo.jpeg')}}" class="w-100" height="320px">
				<h5 class="mt-3">Flamingo</h5>
				<h6 class="text-secondary">(12 pink roses, 1 stargazer, eucalyptus, misty)</h6>
				<h6 class="text-secondary">₱2,400</h6>
			</div>
		</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<center>
			<div style="color:#b82171">
			<img data-enlargable style="cursor: zoom-in" src="{{asset('/images/not.jpg')}}" class="w-100" height="320px">
				<h5 class="mt-3">Iris</h5>
				<h6 class="text-secondary">(12 yellow roses, 1 stargazer, eucalyptus, misty)</h6>
				<h6 class="text-secondary">₱2,400</h6>
			</div>
		</center>
	</div>


	<div class="col-12 col-md-6 mt-5">
		<h6 class="text-secondary">
			Select from our different gorgeous mixed bouquets of fragrant roses, stunning lilies, aromatic eucalyptus and dainty misty. It will truly bring a sweet smile to anyone.
			<br><br>
			Every arm bouquet is packaged in a classy water- resistant wrapper with a matching ribbon. Each stem is kept fresh with water stored in a floral tube or floral foam. It also comes with a greeting card where you can put into words how you feel.
		@include('sections.delivery')
		<br>
		@include('sections.novase')
		<br><br>
		@include('sections.order')
		</h6>
	</div>

</div><!-- end of row -->

<script>
$('img[data-enlargable]').addClass('img-enlargable').click(function(){
    var src = $(this).attr('src');
    $('<div>').css({
        background: 'RGBA(0,0,0,.5) url('+src+') no-repeat center',
        backgroundSize: 'contain',
        width:'100%', height:'100%',
        position:'fixed',
        zIndex:'10000',
        top:'0', left:'0',
        cursor: 'zoom-out'
    }).click(function(){
        $(this).remove();
    }).appendTo('body');
});
</script>
@endsection
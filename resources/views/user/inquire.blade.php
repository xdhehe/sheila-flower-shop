<!-- SLIDESHOW CDN -->
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

@extends('layouts.user')
@section('title', 'Sheilablossoms Service Inquire')
@section('content')

<div class="card p-2 pt-3 mt-4">
  <center><h1 class="mb-2 m-2">{{$service->title}}</h1></center>
  <hr><br>

@if(count($photos) > 0)
  <?php $count= 1; ?>
    <div class="w3-content" style="max-width:1200px">
    @foreach($photos as $photo)
    <?php $path = "images/services/$service->id/$photo->name"?>
      <img class="mySlides w3-animate-opacity img-fluid" src="{{asset($path)}}" style="display:none; max-height: 600px">
    @endforeach
    </div>    
    <div pr-5 pl-5">
      <div class="row mt-4">
        @foreach($photos as $photo)
        <?php $path = "images/services/$service->id/$photo->name"?>
              <div class="col-3">
                  <img class="demo w3-opacity w3-hover-opacity-off w-75 mt-3" src="{{asset($path)}}" height="100px" style="cursor:pointer;" onclick="currentDiv(<?php echo $count;?>)">
              </div>
          <?php $count++;?>
        @endforeach
      </div>
    </div>
</div><!-- end of card -->

  <h5 class="m-2 mt-4 text-secondary"><?php echo nl2br($service->description)?></h5>
   &nbsp;&nbsp;<button class="btn btn-white mt-3" style="background-color: #84cb4d">
      <h6 class="text-white">Inquire Now</h6>
    </button>
  <hr>
@endif

<script>
var myIndex = 0;
carousel();

function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";  
    }
    myIndex++;
    if (myIndex > x.length) {myIndex = 1}    
    x[myIndex-1].style.display = "block";  
    setTimeout(carousel, 10000); // Change image every 2 seconds
}


function currentDiv(n) {
  showDivs(slideIndex = n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  if (n > x.length) {slideIndex = 1}
  if (n < 1) {slideIndex = x.length}

  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";
  }

  for (i = 0; i < dots.length; i++) {
     dots[i].className = dots[i].className.replace(" w3-opacity-off", "");
  }
  x[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " w3-opacity-off";
}
</script>
@endsection
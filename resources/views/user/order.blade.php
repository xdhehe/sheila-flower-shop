@extends('layouts.user')
@section('title', 'Sheilablossoms Order')
@section('content')
<div class="card p-3">
	<div class="row p-4"> 
		<div class="col-sm-12 col-md-6">
			<center><h2 class="text-success"><b>{{$flower->name}}</b></h2></center><hr>
			<?php $imgSource = "/images/flowers/$flower->category_id/$flower->picture";?>
			<img data-enlargable class="w-100" style="cursor: zoom-in"  src="{{asset($imgSource)}}"/><hr>
			<h5>
				<?php echo nl2br($flower->description)?>
			</h5><hr>

			<center>
				<button type="button" class="btn btn-success mb-3 text-white">
			  		<h5 class="p-2"></i> Local Flower</h5>
				</button>					

				<button type="button" class="btn btn-success mb-3 text-white">
			  		<h5 class="p-2"></i> Imported Flower</h5>
				</button>								
			</center>
	
			<hr>

		</div><!-- end of col -->
		
		<div class="col-sm-12 col-md-6">
			<h4 class="text-primary align-left mb-2"><b>Price: {{$flower->price}}</b></h4>
			<h5>Note: we can adjust depends on your budget</h5>
			<hr>

			<img src="{{asset('images/general/mode of payment.jpg')}}" width="100%"><br><br><hr>

			<center><!-- Button trigger modal -->
				  <a href="http://bit.ly/SheilasOrderForm" target="_blank">
					<button type="button" class="btn btn-success mb-3 text-white">
				  		<h5 class="p-2"><i class="fas fa-shopping-cart"></i> Order Now</h5>
					</button>
				  </a>
				  
				<a href="{{url('/contact_us')}}" target="_blank">
					<button type="button" class="btn btn-info text-white mb-3">
				  <h5 class="p-2"><i class="fas fa-info"></i> Questions? Contact Us</h5>
				</button></a>
			</center>
			<hr><br>
		</div><!-- end of col -->
	</div><!-- end of row -->

	<div class="row">
		<div class="col-sm-12 col-md-6">
			<center>
				<h4><i class="fab fa-facebook-square"></i>
					<b class="text-dark"> Like us and Follow us</b>
					<i class="fab fa-instagram"></i>
				</h4>
					@include('sections.socialmedia')
				<hr>
			</center>
		</div><!-- end of col -->

		<div class="col-sm-12 col-md-6">

		</div><!-- end of col -->
	</div><!-- end of row -->
</div><!-- End of card-->

<script>
$('img[data-enlargable]').addClass('img-enlargable').click(function(){
    var src = $(this).attr('src');
    $('<div>').css({
        background: 'RGBA(0,0,0,.5) url('+src+') no-repeat center',
        backgroundSize: 'contain',
        width:'100%', height:'100%',
        position:'fixed',
        zIndex:'10000',
        top:'0', left:'0',
        cursor: 'zoom-out'
    }).click(function(){
        $(this).remove();
    }).appendTo('body');
});	
</script>
@endsection

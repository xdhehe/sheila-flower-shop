@extends('layouts.user')
@section('title', 'Sheilablossoms You are My Sunshine')
@section('content')
<div class="card p-4">
	<small>
		<a href="{{url('/')}}" class="text-secondary">Home</a> /&nbsp;
		<a href="{{url('/')}}" class="text-secondary">Flowers</a> /&nbsp;
		<a href="{{url('/valentines')}}" class="text-secondary">Valentine's Special</a> /&nbsp;
		You Are My Sunshine
	</small>
	<h4 class="text-success mt-3">
		<b style="color:#b82171">You Are My Sunshine</b><br>
		<h6 style="color:#b82171"><b>₱1,299</b></h6>
	</h4>

	<div class="row"> 
		<div class="col-12 col-md-4 mt-4">
			<img data-enlargable class="w-100" style="cursor: zoom-in"  src="{{asset('images/flowers/valentine/sun.jpeg')}}" height="400px">
			<hr>
		</div><!-- end of col -->

		<div class="col-12 col-md-4 mt-4">
			<h6 class="text-secondary">
				Valentine’s Day is the perfect time of the year to show the person you love how much you truly care. Make her heart sing with these fragrant flowers, complemented with humble yet heartfelt gifts.
				@include('sections.delivery')
				<br>
				@include('sections.order')
			</h6>
			
		</div><!-- end of col -->		

	</div><!-- end of row -->

</div><!-- End of card-->

<script>
$('img[data-enlargable]').addClass('img-enlargable').click(function(){
    var src = $(this).attr('src');
    $('<div>').css({
        background: 'RGBA(0,0,0,.5) url('+src+') no-repeat center',
        backgroundSize: 'contain',
        width:'100%', height:'100%',
        position:'fixed',
        zIndex:'10000',
        top:'0', left:'0',
        cursor: 'zoom-out'
    }).click(function(){
        $(this).remove();
    }).appendTo('body');
});	
</script>
@endsection

@extends('layouts.user')
@section('title', 'Sheilablossoms 2020 Valentines Special')
@section('content')
<div class="card p-4">
	<small>
		<a href="{{url('/')}}" class="text-secondary">Home</a> /&nbsp;
		<a href="{{url('/')}}" class="text-secondary">Flowers</a> /&nbsp;
		<a href="{{url('/valentines')}}" class="text-secondary">Valentine's Special</a> /&nbsp;
		2020 Valentine Special
	</small>
	<h4 class="text-success mt-3">
		<b style="color:#b82171">All For Love</b><br>
		<h6 style="color:#b82171"><b>₱2,800</b></h6>
	</h4>

	<div class="row">
		<div class="col-12 col-md-4 mt-4">
			<img data-enlargable class="w-100" style="cursor: zoom-in"  src="{{asset('images/flowers/valentine/vd1-1.png')}}" height="400px">
			<hr>
		</div><!-- end of col -->

		<div class="col-12 col-md-4 mt-4">
			<img data-enlargable class="w-100" style="cursor: zoom-in"  src="{{asset('images/flowers/valentine/vd1.png')}}" height="400px">
			<hr>
		</div><!-- end of col -->

		<div class="col-12 col-md-4 mt-4">
			<h6 class="text-secondary">
				She is definitely a unique person who deserves a one-of-a-kind present this Valentine's Season. This special one and two layer acrylic memento may be use to keep your jewelries, lovely pictures and other keepsakes. It comes in square and heart shapes, to match the occasion.
				@include('sections.delivery')
				<br>
				@include('sections.order')
			</h6>

		</div><!-- end of col -->

	</div><!-- end of row -->

</div><!-- End of card-->

<script>
$('img[data-enlargable]').addClass('img-enlargable').click(function(){
    var src = $(this).attr('src');
    $('<div>').css({
        background: 'RGBA(0,0,0,.5) url('+src+') no-repeat center',
        backgroundSize: 'contain',
        width:'100%', height:'100%',
        position:'fixed',
        zIndex:'10000',
        top:'0', left:'0',
        cursor: 'zoom-out'
    }).click(function(){
        $(this).remove();
    }).appendTo('body');
});
</script>
@endsection

@extends('layouts.user')
@section('title', 'Sheilablossoms Valentine’s Specials')
@section('content')

<div class="mt-2">
	<small>
		<a href="{{url('/')}}" class="text-secondary">Home</a> /&nbsp;
		<a href="{{url('/')}}" class="text-secondary">Flowers</a> /&nbsp;
		Valentine's Special
	</small>
</div>


<div class="row">
	<div class="col-6 col-md-3 mt-5">
		<center>
			<div>
				<a href="{{url('/valentines/valentines-speical-1')}}" style="color:#b82171">
					<img src="{{asset('images/flowers/valentine/vd1-1.png')}}" class="w-100" height="300px">
					<h6 class="mt-3">All For Love</h6>
				</a>
				<h6 class="text-secondary">₱2,800</h6>
				<h6 class="text-secondary">(6 Ferrero chocolates, 6 roses)</h6>
			</div>
		</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<center>
			<div>
				<a href="{{url('/valentines/valentines-speical-2')}}" style="color:#b82171">
					<img src="{{asset('images/flowers/valentine/vd2-1.png')}}" class="w-100" height="300px">
					<h6 class="mt-3">The Love Box</h6>
				</a>
				<h6 class="text-secondary">₱2,990</h6>
				<h6 class="text-secondary">(12 Ferrero chocolates, 6 roses, 3 Hershey's chocolate)</h6>
			</div>
		</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<center>
			<div>
				<a href="{{url('/valentines/valentines-speical-3')}}" style="color:#b82171">
					<img src="{{asset('images/flowers/valentine/vd1-2.png')}}" class="w-100" height="300px">
					<h6 class="mt-3">Heart of Roses</h6>
				</a>
				<h6 class="text-secondary">₱2,300</h6>
				<h6 class="text-secondary">(Three large bud imported roses)</h6>
				<span class="text-secondary" style="font-size: 12px;">(Note: Acryllic boxed specials limited supply.)</span>
			</div>
		</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<center>
			<div>
				<a href="{{url('/valentines/valentines-speical-4')}}" style="color:#b82171">
					<img src="{{asset('images/flowers/valentine/vd3.png')}}" class="w-100" height="300px">
					<h6 class="mt-3">Small Bear	</h6>
				</a>
				<h6 class="text-secondary">₱2,350</h6>
				<h6 class="text-secondary">(3 imported yellow roses, 6 Hershey's chocolate, 5 Ferrero chocolates)</h6>
			</div>
		</center>
	</div>

</div><!-- end of row -->
@endsection
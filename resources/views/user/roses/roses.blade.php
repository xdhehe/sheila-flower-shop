@extends('layouts.user')
@section('title', 'Sheilablossoms Roses')
@section('content')
<div class="mt-2">
	<small>
		<a href="{{url('/')}}" class="text-secondary">Home</a> /&nbsp;
		<a href="{{url('/')}}" class="text-secondary">Flowers</a> /&nbsp;
		Roses
	</small>
</div>
<div class="row">
	<div class="col-6 col-md-3 mt-5">
		<a style="color:#b82171" href="{{ url('/roses/romance')}}">
			<img src="{{asset('images/flowers/roses/romance/rh.jpeg')}}" class="w-100" height="320px">
			<center>
			<h4 class="mt-3">Romance</h4>
		</a>
				<h6 class="text-secondary">3-stem Roses</h6>
			</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<a style="color:#b82171" href="{{ url('/roses/passion')}}">
			<img src="{{asset('images/flowers/roses/passion/oh.jpeg')}}" class="w-100" height="320px">
			<center>
				<h4 class="mt-3">Passion</h4>
		</a>
				<h6 class="text-secondary">6-stem Roses</h6>
			</center>
		
	</div>	

	<div class="col-6 col-md-3 mt-5">
		<a style="color:#b82171" href="{{ url('/roses/luxe')}}">
			<img src="{{asset('images/flowers/roses/luxe/ph.jpeg')}}" class="w-100" height="320px">
			<center>			
				<h4 class="mt-3">Luxe</h4>
		</a>
				<h6 class="text-secondary">12-stem Roses</h6>
			</center>
	</div>	

	<div class="col-6 col-md-3 mt-5">
		<a style="color:#b82171" href="{{ url('/roses/supreme')}}">
			<img src="{{asset('images/flowers/roses/supreme/yh.jpeg')}}" class="w-100" height="320px">
			<center>
				<h4 class="mt-3">Supreme</h4>
		</a>
				<h6 class="text-secondary">24-stem Roses</h6>
			</center>
	</div>	

</div><!-- end of row -->
@endsection
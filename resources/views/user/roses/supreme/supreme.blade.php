@extends('layouts.user')
@section('title', 'Sheilablossoms Roses Supreme')
@section('content')
<div class="mt-2">
	<small>
		<a href="{{url('/')}}" class="text-secondary">Home</a> /&nbsp;
		<a href="{{url('/')}}" class="text-secondary">Flowers</a> /&nbsp;
		<a href="{{url('/roses')}}" class="text-secondary">Roses</a> /&nbsp;
		Supreme
	</small>
</div>

<div class="row">
	<div class="col-6 col-md-3 mt-5">
		<a href="{{asset('/roses/supreme/red')}}" class="text-secondary">
			<img src="{{asset('images/not.jpg')}}" class="w-100" height="320px">
			<center>
				<h5 style="color:#b82171" class="mt-3">Lava Red Supreme</h5>
		</a>
			<div class="text-secondary">
				<h6>24-stem Roses</h6>
				<h6>₱2,500</h6>
			</div>
			</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<a href="{{asset('/roses/supreme/pink')}}" class="text-secondary">
			<img src="{{asset('images/not.jpg')}}" class="w-100" height="320px">
			<center>
				<h5 style="color:#b82171" class="mt-3">Amaranth Pink Supreme</h5>
		</a>
			<div class="text-secondary">
				<h6>24-stem Roses</h6>
				<h6>₱2,500</h6>
			</div>
			</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<a href="{{asset('/roses/supreme/yellow')}}" class="text-secondary">
			<div class="notify-badge">curently<br>sold out</div>
			<img src="{{asset('images/flowers/roses/supreme/yh.jpeg')}}" class="w-100" height="320px">
			<center>
				<h5 style="color:#b82171" class="mt-3">Tuscany Yellow Supreme</h5>
		</a>
			<div class="text-secondary">
				<h6>24-stem Roses</h6>
				<h6>₱2,500</h6>
			</div>
			</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<a href="{{asset('/roses/supreme/white')}}" class="text-secondary">
			<img src="{{asset('images/not.jpg')}}" class="w-100" height="320px">
			<center>
				<h5 style="color:#b82171" class="mt-3">Chiffon White Supreme</h5>
		</a>
			<div class="text-secondary">
				<h6>24-stem Roses</h6>
				<h6>₱2,500</h6>
			</div>
			</center>
	</div>
</div><!-- end of row -->

@endsection
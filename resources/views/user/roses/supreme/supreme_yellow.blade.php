@extends('layouts.user')
@section('title', 'Sheilablossoms Roses Tuscany Yellow Supreme')
@section('content')
<div class="card p-4">
	<small>
		<a href="{{url('/')}}" class="text-secondary">Home</a> /&nbsp;
		<a href="{{url('/')}}" class="text-secondary">Flowers</a> /&nbsp;
		<a href="{{url('/roses')}}" class="text-secondary">Roses</a> /&nbsp;
		<a href="{{url('/roses/supreme')}}" class="text-secondary">Supreme</a> /&nbsp;
		Tuscany Yellow Supreme
	</small>

	<h4 class="text-success mt-3">
		<b style="color:#b82171">Tuscany Yellow Supreme</b><br>
		<h6 class="text-secondary">(Home-Grown Roses)</h6>
		<h6 style="color:#b82171"><b>₱2,500</b></h6>
	</h4>
	<div class="row">
		<div class="col-12 col-md-4 mt-4">
			<img data-enlargable class="w-100" style="cursor: zoom-in"  src="{{asset('images/flowers/roses/supreme/yh.jpeg')}}"/ height="400px">
			<hr>
		</div><!-- end of col -->

		<div class="col-12 col-md-4 mt-4">
			<h6 class="text-secondary">
				Yellow roses are perfect gifts for friends to convey how much you appreciate their friendship. It shows care, delight and also symbolizes new beginnings.
				<br><br>
				Every arm bouquet is packaged in a classy water- resistant wrapper with a matching ribbon. Each stem is kept fresh with water stored in a floral tube or floral foam. It also comes with a greeting card where you can put into words how you feel.

				@include('sections.delivery')
				<br>
				@php
				$style = "supreme";
				$color = "";
				@endphp
				@include('sections.novase2')
				<br><br>
				@include('sections.order')
			</h6>

		</div><!-- end of col -->

	</div><!-- end of row -->

</div><!-- End of card-->

<script>
$('img[data-enlargable]').addClass('img-enlargable').click(function(){
    var src = $(this).attr('src');
    $('<div>').css({
        background: 'RGBA(0,0,0,.5) url('+src+') no-repeat center',
        backgroundSize: 'contain',
        width:'100%', height:'100%',
        position:'fixed',
        zIndex:'10000',
        top:'0', left:'0',
        cursor: 'zoom-out'
    }).click(function(){
        $(this).remove();
    }).appendTo('body');
});
</script>
@endsection

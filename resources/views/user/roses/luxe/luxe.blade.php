@extends('layouts.user')
@section('title', 'Sheilablossoms Roses Luxe')
@section('content')
<div class="mt-2">
	<small>
		<a href="{{url('/')}}" class="text-secondary">Home</a> /&nbsp;
		<a href="{{url('/')}}" class="text-secondary">Flowers</a> /&nbsp;
		<a href="{{url('/roses')}}" class="text-secondary">Roses</a> /&nbsp;
		Luxe
	</small>
</div>
<div class="row">
	<div class="col-6 col-md-3 mt-5">
		<a href="{{asset('/roses/luxe/red')}}" class="text-secondary">
			<img src="{{asset('images/flowers/roses/luxe/rh.jpeg')}}" class="w-100" height="320px">
			<center>
				<h5 style="color:#b82171" class="mt-3">Lava Red Luxe</h5>
		</a>
			<div class="text-secondary">
				<h6>12-stem Roses</h6>
				<h6>₱2,200</h6>
			</div>
			</center>
	</div>


	<div class="col-6 col-md-3 mt-5">
		<a href="{{asset('/roses/luxe/yellow')}}" class="text-secondary">
			<img src="{{asset('images/flowers/roses/luxe/yh.jpeg')}}" class="w-100" height="320px">
			<center>
				<h5 style="color:#b82171" class="mt-3">Tuscany Yellow Luxe</h5>
		</a>
			<div class="text-secondary">
				<h6>12-stem Roses</h6>
				<h6>₱2,200</h6>
			</div>
			</center>
	</div>
	<div class="col-6 col-md-3 mt-5">
		<a href="{{asset('/roses/luxe/pink')}}" class="text-secondary">
			<img src="{{asset('images/flowers/roses/luxe/ph.jpeg')}}" class="w-100" height="320px">
			<center>
				<h5 style="color:#b82171" class="mt-3">Amaranth Pink Luxe</h5>
		</a>
			<div class="text-secondary">
				<h6>12-stem Roses</h6>
				<h6>₱2,200</h6>
			</div>
			</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<a href="{{asset('/roses/luxe/white')}}" class="text-secondary">
			<img src="{{asset('images/flowers/roses/luxe/wh.jpeg')}}" class="w-100" height="320px">
			<center>
				<h5 style="color:#b82171" class="mt-3">Chiffon White Luxe</h5>
		</a>
			<div class="text-secondary">
				<h6>12-stem Roses</h6>
				<h6>₱2,200</h6>
			</div>
			</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<a href="{{asset('/roses/luxe/peach')}}" class="text-secondary">
			<img src="{{asset('images/flowers/roses/luxe/pch.jpeg')}}" class="w-100" height="320px">
			<center>
				<h5 style="color:#b82171" class="mt-3">Autumn Peach Luxe</h5>
		</a>
			<div class="text-secondary">
				<h6>12-stem Roses</h6>
				<h6>₱2,200</h6>
			</div>
			</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<a href="{{asset('/roses/luxe/orange')}}" class="text-secondary">
			<div class="notify-badge">curently<br>sold out</div>
			<img src="{{asset('images/flowers/roses/luxe/oh.jpeg')}}" class="w-100" height="320px">
			<center>
				<h5 style="color:#b82171" class="mt-3">Blazing Orange Luxe</h5>
		</a>
			<div class="text-secondary">
				<h6>12-stem Roses</h6>
				<h6>₱2,200</h6>
			</div>
			</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<a href="{{asset('/roses/luxe/coral')}}" class="text-secondary">
			<img src="{{asset('images/flowers/roses/luxe/coral.jpeg')}}" class="w-100" height="320px">
			<center>
				<h5 style="color:#b82171" class="mt-3">Coral Luxe</h5>
		</a>
			<div class="text-secondary">
				<h6>12-stem Roses</h6>
				<h6>₱2,200</h6>
			</div>
			</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<a href="{{asset('/roses/luxe/amber')}}" class="text-secondary">
			<img src="{{asset('images/flowers/roses/luxe/amber.jpeg')}}" class="w-100" height="320px">
			<center>
				<h5 style="color:#b82171" class="mt-3">Amber Luxe</h5>
		</a>
			<div class="text-secondary">
				<h6>12-stem Roses</h6>
				<h6>₱2,200</h6>
			</div>
			</center>
	</div>
	<div class="col-6 col-md-3 mt-5">
		<a href="{{asset('/roses/luxe/sparkle')}}" class="text-secondary">
			<img src="{{asset('images/flowers/roses/luxe/sparkle.jpeg')}}" class="w-100" height="320px">
			<center>
				<h5 style="color:#b82171" class="mt-3">Sparkle Luxe</h5>
		</a>
			<div class="text-secondary">
				<h6>12-stem Roses</h6>
				<h6>₱2,200</h6>
			</div>
			</center>
	</div>


	<div class="col-6 col-md-3 mt-5">
		<a href="{{asset('/roses/luxe/twilight')}}" class="text-secondary">
			<div class="notify-badge">curently<br>sold out</div>
			<img src="{{asset('images/not.jpg')}}" class="w-100" height="320px">
			<center>
				<h5 style="color:#b82171" class="mt-3">Twilight Luxe(Peach & Orange)</h5>
		</a>
			<div class="text-secondary">
				<h6>12-stem Roses</h6>
				<h6>₱2,200</h6>
			</div>
			</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<a href="{{asset('/roses/luxe/twinkle')}}" class="text-secondary">
			<img src="{{asset('images/not.jpg')}}" class="w-100" height="320px">
			<center>
				<h5 style="color:#b82171" class="mt-3">Twinkle Luxe(Pink & Yellow)</h5>
		</a>
			<div class="text-secondary">
				<h6>12-stem Roses</h6>
				<h6>₱2,200</h6>
			</div>
			</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<a href="{{asset('/roses/luxe/shimmer')}}" class="text-secondary">
			<img src="{{asset('images/not.jpg')}}" class="w-100" height="320px">
			<center>
				<h5 style="color:#b82171" class="mt-3">Shimmer Luxe(Red & Orange)</h5>
		</a>
			<div class="text-secondary">
				<h6>12-stem Roses</h6>
				<h6>₱2,200</h6>
			</div>
			</center>
	</div>




</div><!-- end of row -->
@endsection
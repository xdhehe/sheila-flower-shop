@extends('layouts.user')
@section('title', 'Sheilablossoms Roses Romance')
@section('content')
<div class="mt-2">
	<small>
		<a href="{{url('/')}}" class="text-secondary">Home</a> /&nbsp;
		<a href="{{url('/')}}" class="text-secondary">Flowers</a> /&nbsp;
		<a href="{{url('/roses')}}" class="text-secondary">Roses</a> /&nbsp;
		Romance
	</small>
</div>

<div class="row mobileImageFriendly">
	<div class="col-6 col-md-3 mt-5">
		<a href="{{asset('/roses/romance/red')}}" class="text-secondary">
			<img src="{{asset('images/flowers/roses/romance/rh.jpeg')}}" class="w-100" height="320px">
			<center>
				<h5 style="color:#b82171" class="mt-3">Red Romance</h5>
		</a>
			<div class="text-secondary">
				<h6>3-stem Roses</h6>
				<h6>₱600</h6>
			</div>
			</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<a href="{{asset('/roses/romance/white')}}" class="text-secondary">
			<img src="{{asset('images/flowers/roses/romance/wh.jpeg')}}" class="w-100" height="320px">
			<center>
				<h5 style="color:#b82171" class="mt-3">White Romance</h5>
		</a>
			<div class="text-secondary">
				<h6>3-stem Roses</h6>
				<h6>₱600</h6>
			</div>
			</center>
	</div>


	<div class="col-6 col-md-3 mt-5">
		<a href="{{asset('/roses/romance/pink')}}" class="text-secondary">
			<img src="{{asset('images/flowers/roses/romance/ph.jpeg')}}" class="w-100" height="320px">
			<center>
				<h5 style="color:#b82171" class="mt-3">Pink Romance</h5>
		</a>
			<div class="text-secondary">
				<h6>3-stem Roses</h6>
				<h6>₱600</h6>
			</div>
			</center>
	</div>
	<div class="col-6 col-md-3 mt-5">
		<a href="{{asset('/roses/romance/yellow')}}" class="text-secondary">
			<div class="notify-badge">curently<br>sold out</div>
			<img src="{{asset('images/flowers/roses/romance/yh.jpeg')}}" class="w-100" height="320px">
			<center>
				<h5 style="color:#b82171" class="mt-3">Yellow Romance</h5>
		</a>
			<div class="text-secondary">
				<h6>3-stem Roses</h6>
				<h6>₱600</h6>
			</div>
			</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<a href="{{asset('/roses/romance/peach')}}" class="text-secondary">
			<div class="notify-badge">curently<br>sold out</div>
			<img src="{{asset('images/flowers/roses/romance/pch.jpeg')}}" class="w-100" height="320px">
			<center>
				<h5 style="color:#b82171" class="mt-3">Peach Romance</h5>
		</a>
			<div class="text-secondary">
				<h6>3-stem Roses</h6>
				<h6>₱600</h6>
			</div>
			</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<a href="{{asset('/roses/romance/orange')}}" class="text-secondary">
			<div class="notify-badge">curently<br>sold out</div>
			<img src="{{asset('images/flowers/roses/romance/oh.jpeg')}}" class="w-100" height="320px">
			<center>
				<h5 style="color:#b82171" class="mt-3">Orange Romance</h5>
		</a>
			<div class="text-secondary">
				<h6>3-stem Roses</h6>
				<h6>₱600</h6>
			</div>
			</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<a href="{{asset('/roses/romance/perwinkle')}}" class="text-secondary">
			<img src="{{asset('images/not.jpg')}}" class="w-100" height="320px">
			<center>
				<h5 style="color:#b82171" class="mt-3">Periwinkle Blue</h5>
		</a>
			<div class="text-secondary">
				<h6>3-stem Roses</h6>
				<h6>₱1,650</h6>
			</div>
			</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<a href="{{asset('/roses/romance/simone')}}" class="text-secondary">
			<img src="{{asset('images/flowers/roses/romance/simone.jpeg')}}" class="w-100" height="320px">
			<center>
				<h5 style="color:#b82171" class="mt-3">Two-Toned Simone</h5>
		</a>
			<div class="text-secondary">
				<h6>3-stem Roses</h6>
				<h6>₱1,650</h6>
			</div>
			</center>
	</div>


</div><!-- end of row -->

@endsection
@extends('layouts.user')
@section('title', 'Sheilablossoms Roses Passion')
@section('content')
<div class="mt-2">
	<small>
		<a href="{{url('/')}}" class="text-secondary">Home</a> /&nbsp;
		<a href="{{url('/')}}" class="text-secondary">Flowers</a> /&nbsp;
		<a href="{{url('/roses')}}" class="text-secondary">Roses</a> /&nbsp;
		Passion
	</small>
</div>

<div class="row">
	<div class="col-6 col-md-3 mt-5">
		<a href="{{asset('/roses/passion/red')}}" class="text-secondary">
			<img src="{{asset('images/flowers/roses/passion/rh.jpeg')}}" class="w-100" height="320px">
			<center>
				<h5 style="color:#b82171" class="mt-3">Red Passion</h5>
		</a>
			<div class="text-secondary">
				<h6>6-stem Roses</h6>
				<h6>₱1,200</h6>
			</div>
			</center>
	</div>
	<div class="col-6 col-md-3 mt-5">
		<a href="{{asset('/roses/passion/white')}}" class="text-secondary">
			<img src="{{asset('images/flowers/roses/passion/wh.jpeg')}}" class="w-100" height="320px">
			<center>
				<h5 style="color:#b82171" class="mt-3">White Passion</h5>
		</a>
			<div class="text-secondary">
				<h6>6-stem Roses</h6>
				<h6>₱1,200</h6>
			</div>
			</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<a href="{{asset('/roses/passion/pink')}}" class="text-secondary">
			<img src="{{asset('images/flowers/roses/passion/ph.jpeg')}}" class="w-100" height="320px">
			<center>
				<h5 style="color:#b82171" class="mt-3">Pink Passion</h5>
		</a>
			<div class="text-secondary">
				<h6>6-stem Roses</h6>
				<h6>₱1,200</h6>
			</div>
			</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<a href="{{asset('/roses/passion/yellow')}}" class="text-secondary">
			<div class="notify-badge">curently<br>sold out</div>
			<img src="{{asset('images/flowers/roses/passion/yh.jpeg')}}" class="w-100" height="320px">
			<center>
				<h5 style="color:#b82171" class="mt-3">Yellow Passion</h5>
		</a>
			<div class="text-secondary">
				<h6>6-stem Roses</h6>
				<h6>₱1,200</h6>
			</div>
			</center>
	</div>


	<div class="col-6 col-md-3 mt-5">
		<a href="{{asset('/roses/passion/peach')}}" class="text-secondary">
			<div class="notify-badge">curently<br>sold out</div>
			<img src="{{asset('images/flowers/roses/passion/pch.jpeg')}}" class="w-100" height="320px">
			<center>
				<h5 style="color:#b82171" class="mt-3">Peach Passion</h5>
		</a>
			<div class="text-secondary">
				<h6>6-stem Roses</h6>
				<h6>₱1,200</h6>
			</div>
			</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<a href="{{asset('/roses/passion/orange')}}" class="text-secondary">
			<div class="notify-badge">curently<br>sold out</div>
			<img src="{{asset('images/flowers/roses/passion/oh.jpeg')}}" class="w-100" height="320px">
			<center>
				<h5 style="color:#b82171" class="mt-3">Orange Passion</h5>
		</a>
			<div class="text-secondary">
				<h6>6-stem Roses</h6>
				<h6>₱1,200</h6>
			</div>
			</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<a href="{{asset('/roses/passion/perwinkle')}}" class="text-secondary">
			<img src="{{asset('images/not.jpg')}}" class="w-100" height="320px">
			<center>
				<h5 style="color:#b82171" class="mt-3">Mystery Blue</h5>
		</a>
			<div class="text-secondary">
				<h6>6-stem Roses</h6>
				<h6>₱2,400</h6>
			</div>
			</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<a href="{{asset('/roses/passion/simone')}}" class="text-secondary">
			<img src="{{asset('images/flowers/roses/passion/simone.jpeg')}}" class="w-100" height="320px">
			<center>
				<h5 style="color:#b82171" class="mt-3">Two-toned Victoria</h5>
		</a>
			<div class="text-secondary">
				<h6>6-stem Roses</h6>
				<h6>₱2,400</h6>
			</div>
			</center>
	</div>




</div><!-- end of row -->

@endsection
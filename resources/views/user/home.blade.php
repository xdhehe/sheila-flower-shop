@extends('layouts.user')
<link href="{{asset('/css/style.css')}}" rel="stylesheet" type="text/css">
@section('title', 'Sheilablossoms Home')
@section('content')
@if(count($features) > 0)
  <center>
    <div class="col-md-10 col-sm-12">
      <div class="content" style="overflow:hidden;">
          <div id="slider" style="height:400px">
            @foreach($features as $feature)
            <?php $path = "/images/features/$feature->name"?>
            <ul>
              <li><a href="#"><img src="{{asset($path)}}"></a></li>
            </ul>
            @endforeach
         </div>
      </div>
    </div>
  </center>
@endif
<hr>
  <h2 class="mt-4 text-secondary">Our Story</h2>
  <h5 class="text-secondary">
  SheilaMay’s Blossoms Flower and Gift Shop traces its roots back to 1996, when it first opened as Fresh Blossoms Flower and Gift Shop.<br><br>

  Within its first few months of operations, it was immediately recognized for raising the quality and value of floral and aesthetic arrangements for all occasions not only in Los Banos, Laguna, but in adjoining municipalities and provinces as well.<br><br>

  Twenty-two years hence, Sheila May’s Blossoms Flowers and Gift Shop has continued to advance the art and science of floral design, through continuous learning for its designers and staff on modern techniques and tools for floral arrangements, while ensuring the availability of highest quality and wide-ranging variety of flowers sourced through its broad network of local, national and global providers.<br><br>

  Today, Sheila May’s Blossoms Flowers and Gift Shops is proud to have not only individual clients, but private companies, government organizations, and international organizations, in Laguna, Metro Manila, and neighboring provinces, among its loyal clients.  And we look forward to continuing to provide tailored services according to your needs!
  </h5>


<br><hr>

<div class="row">

  <div class="col-6 col-md-3 mt-3 mb-5">
    <a href="{{url('/valentines')}}">
      <h5 class="card p-2" style="background-color: #ba2473">
        <center><div class="text-white">Valentine’s Specials</div></center>
      </h5>
      <img class="w-100  mobileImageFriendly" src="{{asset('/images/flowers/valentine/vd1-1.png')}}" style="height: 300px">
    </a>
  </div>

  <div class="col-6 col-md-3 mt-3 mb-5">
    <a href="{{url('/roses')}}">
      <h5 class="card p-2" style="background-color: #ba2473">
        <center><div style="color: #ba2473" class="text-white">Roses</div></center>
      </h5>
      <img class="w-100" src="{{asset('images/flowers/roses/luxe/ph.jpeg')}}" style="height: 300px">
    </a>
  </div>

  <div class="col-6 col-md-3 mt-3 mb-5">
    <a href="{{url('/tulips')}}">
      <h5 class="card p-2" style="background-color: #ba2473">
        <center><div class="text-white">Tulips</div></center>
      </h5>
      <img class="w-100" src="{{asset('/images/flowers/tulips/tulips6.jpeg')}}" style="height: 300px">
    </a>
  </div>

  <div class="col-6 col-md-3 mt-3 mb-5">
    <a href="{{url('/sunflower')}}">
      <h5 class="card p-2" style="background-color: #ba2473">
        <center><div class="text-white">Sunflower</div></center>
      </h5>
      <img class="w-100" src="{{asset('images/flowers/sunflower/sf6.jpeg')}}" style="height: 300px">
    </a>
  </div>



  <div class="col-6 col-md-3 mt-3 mb-5">
    <a href="{{url('/mixed')}}">
      <h5 class="card p-2" style="background-color: #ba2473">
        <center><div class="text-white">Mixed Bouquets</div></center>
      </h5>
      <img class="w-100" src="{{asset('/images/flowers/mixed/sangria.jpeg')}}" style="height: 300px">
    </a>
  </div>


  <div class="col-6 col-md-3 mt-3 mb-5">
    <a href="{{url('/spring')}}">
      <h5 class="card p-2" style="background-color: #ba2473">
        <center><div style="color: #ba2473" class="text-white">Spring Bouquets</div></center>
      </h5>
      <img class="w-100" src="{{asset('/images/flowers/spring/carnival.jpeg')}}" style="height: 300px">
    </a>
  </div>

  <!-- <div class="col-6 col-md-3 mt-3 mb-5">
    <a href="{{url('/circle')}}">
      <h5 class="card p-2" style="background-color: #ba2473">
        <center><div class="text-white">Circle of Love Collection</div></center>
      </h5>
      <img class="w-100" src="{{asset('/images/flowers/circle/yellow.jpeg')}}" style="height: 300px">
    </a>
  </div> -->

  <div class="col-6 col-md-3 mt-3 mb-5">
    <a href="{{url('/complete')}}">
      <h5 class="card p-2" style="background-color: #ba2473">
        <center><div class="text-white">You Complete Me Collection</div></center>
      </h5>
      <img class="w-100" src="{{asset('/images/flowers/complete/complete_red.jpeg')}}" style="height: 300px">
    </a>
  </div>
</div><!-- end of row -->
@endsection




@section('js')
  @parent
  <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
  <script type="text/javascript" src="{{asset('/js/vmc.slide.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/vmc.slide.effects.js')}}"></script>

  <script>
//for slide show
$( document ).ready(function() {
    $('#slider').vmcSlide({
        duration: 3000,
        speed: 1000
        , effects: ['fade', 'slideX', 'slideY', 'page', 'page2', 'rollingX', 'rollingY', 'blindsX', 'blindsY']
        , showText: false
        // , random: true
        // ,sideButton:true
        // ,navButton:true
    });
  });

  </script>
@endsection
@extends('layouts.user')
@section('title', 'Sheilablossoms Pastels in Bloom')
@section('content')
<div class="card p-4">
	<small>
		<a href="{{url('/')}}" class="text-secondary">Home</a> /&nbsp;
		<a href="{{url('/')}}" class="text-secondary">Flowers</a> /&nbsp;
		<a href="{{url('/mothers-day')}}" class="text-secondary">Mothers Day</a> /&nbsp;
		Pastels in Bloom
	</small>
	
	<h4 class="text-success mt-3">
		<b style="color:#b82171">Pastels in Bloom</b><br>
		<h6 style="color:#b82171"><b>₱1,599</b></h6>
	</h4>

	<div class="row"> 
		<div class="col-12 col-md-4 mt-4">
			<img data-enlargable class="w-100" style="cursor: zoom-in"  src="{{asset('images/flowers/mother/pastel.jpg')}}"/ height="400px">
			<hr>
		</div><!-- end of col -->

		<div class="col-12 col-md-4 mt-4">
			<h6 class="text-secondary">
				Pastel colors are delicate, feminine and springtime, a perfect present to give to your loving mother this bright month of May. 
				<br><br>
				Our Pastels in Bloom is a nice collection of soft roses, adorable gerberas and robustic mums, all admirable traits of your mother. This special bouquet comes in this customized rustic wooden basket,  a ready and elegant centerpiece that is sure to bring calmness and smiles.
				<br><br>
				<b>Note:</b> The colors of the flowers may differ according to availability but trust us that we will put together the best pastel color combination, just for your best mom.
				
				
				@include('sections.delivery')
				<br>
				<!-- @include('sections.novase3') -->
				<br><br>
				@include('sections.order')
			</h6>
		</div><!-- end of col -->		

	</div><!-- end of row -->

</div><!-- End of card-->

<script>
$('img[data-enlargable]').addClass('img-enlargable').click(function(){
    var src = $(this).attr('src');
    $('<div>').css({
        background: 'RGBA(0,0,0,.5) url('+src+') no-repeat center',
        backgroundSize: 'contain',
        width:'100%', height:'100%',
        position:'fixed',
        zIndex:'10000',
        top:'0', left:'0',
        cursor: 'zoom-out'
    }).click(function(){
        $(this).remove();
    }).appendTo('body');
});	
</script>
@endsection

@extends('layouts.user')
@section('title', 'Sheilablossoms Mothers Day')
@section('content')

<div class="mt-2">
	<small>
		<a href="{{url('/')}}" class="text-secondary">Home</a> /&nbsp;
		<a href="{{url('/')}}" class="text-secondary">Flowers</a> /&nbsp;
		Mother's Day
	</small>
</div>


<div class="row">
	<div class="col-6 col-md-3 mt-5">
		<a style="color:#b82171" href="{{ url('/mothers-day/basket')}}">
			<img src="{{asset('images/flowers/mother/basket.jpg')}}" class="w-100" height="320px">
			<center>
				<h5 class="mt-3">Flowers and Fruits Gift Basket</h5>
		</a>
				<h6 class="text-secondary">₱1,799</h6>
			</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<a style="color:#b82171" href="{{ url('/mothers-day/pastel')}}">
			<img src="{{asset('images/flowers/mother/pastel.jpg')}}" class="w-100" height="320px">
			<center>
				<h5 class="mt-3">Pastels in Bloom</h5>
		</a>
				<h6 class="text-secondary">₱1,599</h6>
			</center>
	</div>	
</div><!-- end of row -->
@endsection
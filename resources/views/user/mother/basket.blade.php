@extends('layouts.user')
@section('title', 'Sheilablossoms Flowers and Fruits Gift Basket')
@section('content')
<div class="card p-4">
	<small>
		<a href="{{url('/')}}" class="text-secondary">Home</a> /&nbsp;
		<a href="{{url('/')}}" class="text-secondary">Flowers</a> /&nbsp;
		<a href="{{url('/mothers-day')}}" class="text-secondary">Mothers Day</a> /&nbsp;
		Flowers and Fruits Gift Basket
	</small>
	
	<h4 class="text-success mt-3">
		<b style="color:#b82171">Flowers and Fruits Gift Basket</b><br>
		<h6 style="color:#b82171"><b>₱1,799</b></h6>
	</h4>

	<div class="row"> 
		<div class="col-12 col-md-4 mt-4">
			<img data-enlargable class="w-100" style="cursor: zoom-in"  src="{{asset('images/flowers/mother/basket.jpg')}}"/ height="400px">
			<hr>
		</div><!-- end of col -->

		<div class="col-12 col-md-4 mt-4">
			<h6 class="text-secondary">
				A Gift Basket always brings delight to everyone, and moms are big fans of it!
				<br><br>
				Treat her sweet tooth with the natural freshness and sweetness of fruits, perfectly matched with these elegant red roses and amazing stargazer, this charming arrangement is meticulously set-up in a 7.5" x 6.5" x 12" customized, handmade wooden basket, a fitting surprise to your wonderful mom! 
				
				
				@include('sections.delivery')
				<br>
				<!-- @include('sections.novase3') -->
				<br><br>
				@include('sections.order')
			</h6>
		</div><!-- end of col -->		

	</div><!-- end of row -->

</div><!-- End of card-->

<script>
$('img[data-enlargable]').addClass('img-enlargable').click(function(){
    var src = $(this).attr('src');
    $('<div>').css({
        background: 'RGBA(0,0,0,.5) url('+src+') no-repeat center',
        backgroundSize: 'contain',
        width:'100%', height:'100%',
        position:'fixed',
        zIndex:'10000',
        top:'0', left:'0',
        cursor: 'zoom-out'
    }).click(function(){
        $(this).remove();
    }).appendTo('body');
});	
</script>
@endsection

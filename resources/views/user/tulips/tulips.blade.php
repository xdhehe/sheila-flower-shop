@extends('layouts.user')
@section('title', 'Sheilablossoms Tulips')
@section('content')
<div class="mt-2">
	<small>
		<a href="{{url('/')}}" class="text-secondary">Home</a> /&nbsp;
		<a href="{{url('/')}}" class="text-secondary">Flowers</a> /&nbsp;
		Tulips
	</small>
</div>
<br>

<div class="row">
	<div class="col-6 col-md-2 mb-3">
		<center>
			<div style="color:#b82171" href="{{ url('/tulips/3')}}">
			<img data-enlargable style="cursor: zoom-in" src="{{asset('images/flowers/tulips/tulips3.jpeg')}}" class="w-100" height="250px">
				<h5 class="mt-3">3-Stem Daphne Tulips</h5>
				<h6 class="text-secondary">₱900</h6>
			</div>
		</center>
	</div>

	<div class="col-6 col-md-2 mb-3">
		<center>
			<div style="color:#b82171" href="{{ url('/tulips/6')}}">
			<img data-enlargable style="cursor: zoom-in" src="{{asset('images/flowers/tulips/tulips6.jpeg')}}" class="w-100" height="250px">
				<h5 class="mt-3">6-Stem Daphne Tulips</h5>
				<h6 class="text-secondary">₱1,500</h6>
		</center>
	</div>


	<div class="col-6 col-md-2 mb-3">
		<center>
			<div style="color:#b82171" href="{{ url('/tulips/12')}}">
			<img data-enlargable style="cursor: zoom-in" src="{{asset('images/flowers/tulips/tulips12.jpeg')}}" class="w-100" height="250px">
				<h5 class="mt-3">12-Stem Daphne Tulips</h5>
				<h6 class="text-secondary">₱3,000</h6>
			</div>
		</center>
	</div>

	<div class="col-12 col-md-6">
		<h6 class="text-secondary">
			These beautiful cup-shaped flowers are one of the most unique and popular flowers in the world. Tulips generally mean perfect love. Like other flowers, the different colors of tulips mean different meaning. Red tulips are strongly associated to true love. Yellow tulips symbolize cheer and sunshine. Worthiness and forgiveness are conveyed through white tulips. Bring out the beautiful smile from her face! Choose from three, six to twelve stem tulips to send to your special someone.
			<br><br>
			Every arm bouquet is packaged in a classy water- resistant wrapper with a matching ribbon. Each stem is kept fresh with water stored in a floral tube or floral foam. It also comes with a greeting card where you can put into words how you feel.
			@include('sections.delivery')
			<br>
			@include('sections.novase')
			<br><br>
			@include('sections.order')
		</h6>
	</div><!-- end of col -->
</div><!-- end of Parent row -->

<script>
$('img[data-enlargable]').addClass('img-enlargable').click(function(){
    var src = $(this).attr('src');
    $('<div>').css({
        background: 'RGBA(0,0,0,.5) url('+src+') no-repeat center',
        backgroundSize: 'contain',
        width:'100%', height:'100%',
        position:'fixed',
        zIndex:'10000',
        top:'0', left:'0',
        cursor: 'zoom-out'
    }).click(function(){
        $(this).remove();
    }).appendTo('body');
});
</script>

@endsection
@extends('layouts.user')
@section('title', 'Sheilablossoms Circle')
@section('content')
<div class="mt-2">
	<small>
		<a href="{{url('/')}}" class="text-secondary">Home</a> /&nbsp;
		<a href="{{url('/')}}" class="text-secondary">Flowers</a> /&nbsp;
		Circle of Love Collection
	</small>
</div>
<div class="row">
	<div class="col-6 col-md-3 mt-5">
		<center>
			<div style="color:#b82171">
			<img data-enlargable style="cursor: zoom-in" src="{{asset('images/flowers/circle/red.jpeg')}}" class="w-100" height="270px">
				<h4 class="mt-3">Red Circle of Love</h4>
				<h6 class="text-secondary">(one red Ecuadorian rose)</h6>
				<h6 class="text-secondary">₱989</h6>
			</div>
		</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<center>
			<div style="color:#b82171">
			<img data-enlargable style="cursor: zoom-in" src="{{asset('images/flowers/circle/pink.jpeg')}}" class="w-100" height="270px">
				<h4 class="mt-3">Pink Circle of Love</h4>
				<h6 class="text-secondary">(one pink Ecuadorian rose)</h6>
				<h6 class="text-secondary">₱989</h6>
			</div>
		</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<center>
			<div style="color:#b82171">
			<img data-enlargable style="cursor: zoom-in" src="{{asset('images/flowers/circle/yellow.jpeg')}}" class="w-100" height="270px">
				<h4 class="mt-3">Yellow Circle of Love </h4>
				<h6 class="text-secondary">(one yellow Ecuadorian rose)</h6>
				<h6 class="text-secondary">₱989</h6>
			</div>
		</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<center>
			<div style="color:#b82171">
			<img data-enlargable style="cursor: zoom-in" src="{{asset('images/not.jpg')}}" class="w-100" height="270px">
				<h4 class="mt-3">Orange Circle of Love</h4>
				<h6 class="text-secondary">(one orange Ecuadorian rose)</h6>
				<h6 class="text-secondary">₱989</h6>
			</div>
		</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<center>
			<div style="color:#b82171">
			<img data-enlargable style="cursor: zoom-in" src="{{asset('images/not.jpg')}}" class="w-100" height="270px">
				<h4 class="mt-3">Blue Circle of Love </h4>
				<h6 class="text-secondary">(one blue Ecuadorian rose)</h6>
				<h6 class="text-secondary">₱989</h6>
			</div>
		</center>
	</div>


	<div class="col-12 col-md-8 mt-5">
		<div class="text-secondary mb-3">
			The Circle of Love Collection is a set of different colors of Ecuadorian roses. They are known to be the best roses in the world because of its impressive size. Each lovely bud comes in a special reusable hand-crafted round box and a personalized greeting card.
		</div>
		<h4 style="color: #d279a9">For Pick-up only</h4>
		@include('sections.order')

	</div>
</div><!-- end of row -->

<script>
$('img[data-enlargable]').addClass('img-enlargable').click(function(){
    var src = $(this).attr('src');
    $('<div>').css({
        background: 'RGBA(0,0,0,.5) url('+src+') no-repeat center',
        backgroundSize: 'contain',
        width:'100%', height:'100%',
        position:'fixed',
        zIndex:'10000',
        top:'0', left:'0',
        cursor: 'zoom-out'
    }).click(function(){
        $(this).remove();
    }).appendTo('body');
});	
</script>
@endsection
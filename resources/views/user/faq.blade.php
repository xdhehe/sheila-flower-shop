@extends('layouts.user')
@section('title', 'Sheilablossoms About Us')
@section('content')
<div class="text-secondary">
	<h2 class="mt-3">Frequently Asked Question</h2><br>

	<h4>1.	Who is Sheila May’s Blossoms Flower and Gift Shop?</h4>
	Sheila May’s Blossoms Flower and Gift Shop traces its roots back to 1996, when it first opened as Fresh Blossoms Flower and Gift Shop.<br><br>

	Within its first few months of operations, it was immediately recognized for raising the quality and value of floral and aesthetic arrangements for all occasions not only in Los Banos, Laguna, but in adjoining municipalities and provinces as well.<br><br>

  	Twenty-two years hence, Sheila May’s Blossoms Flowers and Gift Shop has continued to advance the art and science of floral design, through continuous learning for its designers and staff on modern techniques and tools for floral arrangements, while ensuring the availability of highest quality and wide-ranging variety of flowers sourced through its broad network of local, national and global providers.<br><br>

  	Today, Sheila May’s Blossoms Flowers and Gift Shops is proud to have not only individual clients, but private companies, government organizations, and international organizations, in Laguna, Metro Manila, and neighboring provinces, among its loyal clients.  And we look forward to continuing to provide tailored services according to your needs!
	<br><br>

	<h4>2.	How do I place an order?</h4>
	Visit our website www.sheilamaysblossoms.com and choose from our selection of beautiful arrangements specially made for you.<br>
	<br>
	Please place your order details thru this link <a href="http://bit.ly/SheilaMaysOrderForm" target="_blank">http://bit.ly/SheilaMaysOrderForm</a><br>
	In this form, you will find the different types of flower arrangements (do we include the pictures or indicate the website?) that we offer and the payment details.<br>
	Once you have paid, kindly send a picture of your deposit slip to <a href="http://bit.ly/BlossomBusinessMessenger" target="_blank">http://bit/ly/BlossomBusinessMessenger</a><br>
	Indicate on the deposit slip your name and the product name of your order.
	You may also place your order by visiting our shop in Los Banos, Laguna.<br>
	Kindly click <a href="{{ url('/contact_us')}}" class="text-secondary"><b>Contact Us</b></a> for location details.
	<br><br>
	<h4>3.	Is there a price difference between placing an order online and visiting your shop?</h4>
	No. The prices offered are the same.
	<br><br>
	<h4>4.	How do I pay for my order?</h4>
	We are happy to provide you several payment options.<br>
	We accept payments through Cebuana Lhuiller, LBC, Palawan Express, Western Union and through bank deposit.<br>
	You may deposit at BPI under the account of Sheila M.<br>
	Rompe, savings account # 580 635 4025.<br>

	Once you have paid, kindly send a picture of your deposit slip to <a href="http://bit.ly/BlossomBusinessMessenger" target="_blank">http://bit/ly/BlossomBusinessMessenger</a>.<br>
	Indicate on the deposit slip your name and the product name of your order.
	<br><br>
	<h4>5.	Do you accept payment using a credit card?</h4>
	No, we do not.<br>
	However, we would be happy to work on this in the near future.
	<br><br>
	<h4>6.	May I change my order details?</h4>
	We would be happy to accommodate your changes on the product, delivery address, delivery date and message.<br>
	Kindly email us at sheilamayblossoms@icloud.com at least 48 hours prior to the delivery date.<br>

	<br><br>
	<h4>7.	May I cancel my order?</h4>
	Please inform us the cancellation of your order at least 48 hours prior to delivery or pick-up.<br>
	Let us know by emailing us at sheilamayblossoms@icloud.com or by sending us a Viber message at +63 917 300 5685.
	<br><br>
	<div data-toggle="modal" data-target="#idDeliveryFee">
		<h4>8.	Where do you deliver? What are your delivery times?</h4>
		We gladly deliver to selected areas within Laguna.<br>
		All fresh flower orders are delivered personally by Sheila May’s Blossoms Flower and Gift Shop personnel and vehicles.<br><br>
		Please click the <a class="text-secondary" href="#" data-toggle="modal" data-target="#idDeliveryFee"><b>DELIVERY FEES</b></a> to check on the locale and its respective delivery rates.<br>

		Our cut-off time in accepting orders for delivery is until 5:00pm Philippine standard time, for next day delivery.<br>
		There is no guarantee on the exact arrival time but we will grant your request for a morning or afternoon delivery.
		For peak seasons, such as Valentine’s Day and Mother’s Day, we adjust our cut-off times and delivery hours to accommodate properly the volume of orders we receive.
	</div>
	<br>
	<h4>9.	How much do you charge for delivery?</h4>
	Please click on the <a class="text-secondary" href="#" data-toggle="modal" data-target="#idDeliveryFee"><b>DELIVERY FEES</b></a> section to check on the locale and its respective delivery rates.
	<br><br>
	<h4>10.	How would I know if my order has been delivered?</h4>
	Only upon the request of the client, we take pictures of the delivery receipt and the order form and send it to the client as proof of delivery.<br>
	If the recipient is not present during delivery, we take note of the name who received it and its relation to the recipient.<br>

	<br><br>
	<h4>11.	How are your flowers packaged?</h4>
	Every floral bouquet is packaged in a classy water-resistant wrapper with a matching ribbon.<br>
	Each stem is kept fresh with water stored in a floral tube or floral foam.<br>
	It comes with a greeting card where you can put into words how you feel.<br>
	For delivered bouquets, we put each bouquet in our specially designed box to protect it during travel.<br>

	<br><br>
	<h4>12.	Do we get the exact products as shown in the pictures?</h4>
		We do our best to give the exact product as seen in the pictures or as requested. However, due to weather, floral seasons (such as Valentine's and Mother's Day) and availability, substitution of flower variety and color may occur.<br><br>
		In the event that we have to substitute, be assured that we will still provide quality floral and non-floral products, of equal value and beauty. Each floral arrangement is meticulously put together by our designer, to give the great satisfaction that respected customers deserve.
	<br>

<br>
</div>

@endsection
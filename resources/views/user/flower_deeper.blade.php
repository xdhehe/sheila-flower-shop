@extends('layouts.user')
@section('title', 'Sheilablossoms Flower Sub Category')

@section('content')
<div class="row">
	@if(count($flowers)>0)
		@foreach($flowers as $flower)
		<?php $path = "/flowers/$category/$flower->id/order"?>
		    <div class="col-sm-12 col-md-6 mt-5">
				<a href="{{url($path)}}" class="text-dark">
					<div class="card">

					  <div class="card-body text-center">
					  </div>

					  <?php $imgSource = "/images/flowers/$category/$flower->picture" ?>
					  <center>
				  		<img src="{{asset($imgSource)}}" style="width:90%; height: 400px">
					  </center>

					  <div class="card-body"><hr>
					    <p class="card-text"><h4><b>{{$flower->name}}</b></h4></p><hr>
					    <h5>
						    * 3 Stem Red Roses<br>
							* ₱100 Home-Grown Roses<br>
							* ₱200 Ecuadorian Roses
						</h5>
					  </div>
					</div>
				</a>
		    </div>
	    @endforeach
    @endif
</div><!-- end of row -->

<div class="mt-4 d-flex justify-content-center">
 	<h4>{{ $flowers->links() }}</h4>
</div>

@endsection
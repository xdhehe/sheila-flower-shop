@extends('layouts.user')
@section('title', 'Sheilablossoms Search')
@section('content')
@include('sections.messeges')

<div class="row">
	@if(count($services) > 0)
		@foreach($services as $service)
		<?php
			$path = "/services/{$service->title}";
			$imgName = $service->spictures->first()['name'];
			$imgSource ="/images/services/$service->id/$imgName";
		?>
		    <div class="col-sm-12 col-md-6 mt-5">
			<a href="{{$path}}" class="text-dark">
				<div class="card">
				  <div class="card-body text-center">
				    <p class="card-text"><h4>{{$service->title}}</h4></p><hr>
				  </div>
				  <center>
				  	<img src="{{asset($imgSource)}}" style="width:90%; height: 400px">
				  </center><br><br>
				</div>
	    	</a>
		    </div>
	    @endforeach
    @endif
</div><!-- end of row -->

<div class="row">
	@if(count($flowers)>0)
		@foreach($flowers as $flower)
		<?php $path = "/flowers/$flower->category_id/$flower->id/order"?>
		    <div class="col-sm-12 col-md-6 mt-5">
				<a href="{{url($path)}}" class="text-dark">
					<div class="card">

					  <div class="card-body text-center">
					    <p class="card-text"><h4>{{$flower->name}}</h4></p><hr>
					  </div>

					  <?php $imgSource = "/images/flowers/$flower->category_id/$flower->picture" ?>
					  <center>
				  		<img src="{{asset($imgSource)}}" style="width:90%; height: 400px">
					  </center>

					  <div class="card-body"><hr>
					  	<h5 class="text-center">{{$flower->name}}</h5>
					    <p class="card-text text-center">
					    	{{$flower->price}}
					    </p>
					  </div>
					</div>
				</a>
		    </div>
	    @endforeach
    @endif
</div><!-- end of row -->



	@if((count($flowers) == 0 ) && count($services) == 0)
		<div class="col-sm-12 col-md-6 mt-5">
			<div class="card p-3 pb-5">
				<h3 class="ml-2"><br>
					Sorry No Results Found.<br><br>
					Your search - {{$input}} - did not match any documents.<br><br>
					Suggestions:<br>
				</h3>
				<h4 class="ml-4">
					<li>Make Sure all words are spelled correctly.</li>
					<li>Try different keywords.</li>
				</h4>
			</div>
		</div>
	@endif
	</div><!-- end of row -->
@endsection
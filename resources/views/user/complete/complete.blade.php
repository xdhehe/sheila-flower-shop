@extends('layouts.user')
@section('title', 'Sheilablossoms Complete')
@section('content')
<div class="mt-2">
	<small>
		<a href="{{url('/')}}" class="text-secondary">Home</a> /&nbsp;
		<a href="{{url('/')}}" class="text-secondary">Flowers</a> /&nbsp;
		You Complete Me Collection
	</small>
</div>
<div class="row">
	<div class="col-6 col-md-3 mt-5">
		<center>
			<div style="color:#b82171">
			<img data-enlargable style="cursor: zoom-in" src="{{asset('images/flowers/complete/complete_red.jpeg')}}" class="w-100" height="250px">
				<h5 class="mt-3">You Complete Me Red</h4>
				<h6 class="text-secondary">(one dozen red roses with misty)</h6>
				<h6 class="text-secondary">₱1,800</h6>
			</div>
		</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<center>
			<div style="color:#b82171">
			<img data-enlargable style="cursor: zoom-in" src="{{asset('images/flowers/complete/complete_pink.jpeg')}}" class="w-100" height="250px">
				<h5 class="mt-3">You Complete Me Pink </h4>
				<h6 class="text-secondary">(one dozen pink roses with misty)</h6>
				<h6 class="text-secondary">₱1,800</h6>
			</div>
		</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<center>
			<div style="color:#b82171">
			<img data-enlargable style="cursor: zoom-in" src="{{asset('images/not.jpg')}}" class="w-100" height="250px">
				<h5 class="mt-3">You Complete Me Yellow</h4>
				<h6 class="text-secondary">(one dozen yellow roses with misty)</h6>
				<h6 class="text-secondary">₱1,800</h6>
			</div>
		</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<center>
			<div style="color:#b82171">
			<img data-enlargable style="cursor: zoom-in" src="{{asset('images/not.jpg')}}" class="w-100" height="250px">
				<h5 class="mt-3">You Complete Me Orange</h4>
				<h6 class="text-secondary">(one dozen orange roses with misty)</h6>
				<h6 class="text-secondary">₱1,800</h6>
			</div>
		</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<center>
			<div style="color:#b82171">
			<img data-enlargable style="cursor: zoom-in" src="{{asset('images/not.jpg')}}" class="w-100" height="250px">
				<h5 class="mt-3">Perfect Pair of Yellow Orange</h4>
				<h6 class="text-secondary">(six yellow roses, six orange roses, misty)</h6>
				<h6 class="text-secondary">₱1,800</h6>
			</div>
		</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<center>
			<div style="color:#b82171">
			<img data-enlargable style="cursor: zoom-in" src="{{asset('images/not.jpg')}}" class="w-100" height="250px">
				<h5 class="mt-3">Perfect Pair of Yellow Red</h4>
				<h6 class="text-secondary">(six yellow roses, six red roses, misty)</h6>
				<h6 class="text-secondary">₱1,800</h6>
			</div>
		</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<center>
			<div style="color:#b82171">
			<img data-enlargable style="cursor: zoom-in" src="{{asset('images/not.jpg')}}" class="w-100" height="250px">
				<h5 class="mt-3">Perfect Pair of White Pink </h4>
				<h6 class="text-secondary">(six white roses, six pink roses, misty)</h6>
				<h6 class="text-secondary">₱1,800</h6>
			</div>
		</center>
	</div>


	<div class="col-12 col-md-8 mt-5">
		<div class="text-secondary mb-3">
			The You Complete me Collection is a set of our home-grown Holland variety roses. Each arrangement is an elegant ready-to-use centrepiece for your coffee table. It comes in a special reusable hand-crafted square box and a personalized greeting card.
		</div>
		<h4 style="color: #d279a9">For Pick-up only</h4>
		@include('sections.order')

	</div>
</div><!-- end of row -->

<script>
$('img[data-enlargable]').addClass('img-enlargable').click(function(){
    var src = $(this).attr('src');
    $('<div>').css({
        background: 'RGBA(0,0,0,.5) url('+src+') no-repeat center',
        backgroundSize: 'contain',
        width:'100%', height:'100%',
        position:'fixed',
        zIndex:'10000',
        top:'0', left:'0',
        cursor: 'zoom-out'
    }).click(function(){
        $(this).remove();
    }).appendTo('body');
});
</script>
@endsection
@extends('layouts.user')
@section('title', 'Sheilablossoms Services')
@section('content')
<div class="row">
	@if(count($globalServices) > 0)
		@foreach($globalServices as $globalService)
		<?php
			$path = "/services/{$globalService->title}";
			$imgName = $globalService->spictures->first()['name'];
			$imgSource ="/images/services/$globalService->id/$imgName";
		?>
		    <div class="col-sm-12 col-md-6 mt-5">
			<a href="{{$path}}" class="text-dark">
				<div class="card">
				  <div class="card-body text-center">
				    <p class="card-text"><h4>{{$globalService->title}}</h4></p><hr>
				  </div>
				  <center>
				  	<img src="{{asset($imgSource)}}" style="width:90%; max-height: auto">
				  </center><br><br>
				</div>
	    	</a>
		    </div>
	    @endforeach
    @endif
</div><!-- end of row -->
@endsection
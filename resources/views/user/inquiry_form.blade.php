@extends('layouts.user')
@section('title', 'Sheilablossoms Inquiry Form')
@section('content')

<div class="card p-5 mt-5">
	<h1><b>Inquiry Form</b></h1><br>
	<div class="row">
		<div class="col-sm-12 col-md-3">		
			<h3 class="mt-1">Customer Name:</h3>
		</div>
		<div class="col-sm-12 col-md-5">		
			<input type="text" class="form-control" name="name"  placeholder="Name" required>
		</div>
	</div><!-- end of row -->

	<div class="row mt-4">
		<div class="col-sm-12 col-md-3">		
			<h3 class="mt-1">Contact Number: </h3>
		</div>
		<div class="col-sm-12 col-md-5">		
			<input type="text" class="form-control" name="contact" placeholder="Contact Number" required>
		</div>
	</div><!-- end of row -->	

	<div class="row mt-4">
		<div class="col-sm-12 col-md-3">		
			<h3 class="mt-1">Date of Event:</h3>
		</div>
		<div class="col-sm-12 col-md-5">		
			<input type="date" class="form-control" name="date" required>
		</div>
	</div><!-- end of row -->

	<div class="row mt-4">
		<div class="col-sm-12 col-md-3">		
			<h3 class="mt-1">Event Type:</h3>
		</div>
		<div class="col-sm-12 col-md-5">		
			<select class="custom-select" name="event" required>
			  <option value="" selected></option>
			  <option value="Carosa">Carosa</option>
			  <option value="Church">Church</option>
			  <option value="Inauguration">Inauguration</option>
			  <option value="Stage">Stage Arrangement</option>
			  <option value="Wedding">Wedding</option>
			  <option value="Others">Other Events</option>
			</select>
		</div>
		<div class="col-sm-12 col-md-3">
			<input type="text" class="form-control" name="event">
		</div>
	</div><!-- end of row -->	

	<div class="row mt-4">
		<div class="col-sm-12 col-md-3">		
			<h3 class="mt-1">Your Inquiry:</h3>
		</div>
		<div class="col-sm-12 col-md-5">		
			<textarea class="form-control" name="inquiry" rows="6" placeholder="Your Inquiry" required></textarea>
		</div>
	</div><!-- end of row -->	
	<div class="row mt-3">
		<div class="col-sm-12 col-md-3">		
			<h3 class="mt-1"></h3>
		</div>

		<div class="col-5">
			<button class="btn btn-success pl-4 pr-4 float-right">Submit</button>
		</div>
	</div>
</div><!-- end of card -->


	
	Event Type:
	  Carosa
	  Church
	  Inauguration
	  Stage
	  Wedding 
	  Others
	Your Inquiry: 

@endsection
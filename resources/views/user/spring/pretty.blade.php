@extends('layouts.user')
@section('title', 'Sheilablossoms Pretty in Pink')
@section('content')
<div class="card p-4">
	<small>
		<a href="{{url('/')}}" class="text-secondary">Home</a> /&nbsp;
		<a href="{{url('/')}}" class="text-secondary">Flowers</a> /&nbsp;
		<a href="{{url('/spring')}}" class="text-secondary">Spring</a> /&nbsp;
		Pretty in Pink
	</small>
	<h4 class="text-success mt-3">
		<b style="color:#b82171">Pretty in Pink</b><br>
		<h6 class="text-secondary">(3 Imported Pink Roses, 3 pink Gerberas, 3 Pink Carnations)</h6>
		<h6 style="color:#b82171"><b>₱1,700</b></h6>
	</h4>

	<div class="row">
		<div class="col-12 col-md-4 mt-4">
			<img data-enlargable class="w-100" style="cursor: zoom-in"  src="{{asset('images/flowers/spring/pretty.jpeg')}}"/ height="400px">
			<hr>
		</div><!-- end of col -->

		<div class="col-12 col-md-4 mt-4">
			<h6 class="text-secondary">
				This all-pink bouquet is a classic symbol of grace and elegance. Make someone special with this delicate fusion of different shades of pink. It is sure to bring warmth and light to any home!
				<br><br>
				Every flower is meticulously hand-tied together, packaged with a classy water-proof wrapper and a matching ribbon. Each stem is kept fresh with water stored in a floral tube or floral foam. It comes with a greeting card where you can put into words how you feel.

				@include('sections.delivery')
				<br>
				@include('sections.novase3')
				<br><br>
				@include('sections.order')
			</h6>
		</div><!-- end of col -->

	</div><!-- end of row -->

</div><!-- End of card-->

<script>
$('img[data-enlargable]').addClass('img-enlargable').click(function(){
    var src = $(this).attr('src');
    $('<div>').css({
        background: 'RGBA(0,0,0,.5) url('+src+') no-repeat center',
        backgroundSize: 'contain',
        width:'100%', height:'100%',
        position:'fixed',
        zIndex:'10000',
        top:'0', left:'0',
        cursor: 'zoom-out'
    }).click(function(){
        $(this).remove();
    }).appendTo('body');
});
</script>
@endsection

@extends('layouts.user')
@section('title', 'Sheilablossoms Rhapsody Red')
@section('content')
<div class="card p-4">
	<small>
		<a href="{{url('/')}}" class="text-secondary">Home</a> /&nbsp;
		<a href="{{url('/')}}" class="text-secondary">Flowers</a> /&nbsp;
		<a href="{{url('/spring')}}" class="text-secondary">Spring</a> /&nbsp;
		Rhapsody Red
	</small>
	<h4 class="text-success mt-3">
		<b style="color:#b82171">Rhapsody Red</b><br>
		<h6 class="text-secondary">(3 Imported Red Roses, 3 Red Carnations, 3 Red Gerberas)</h6>
		<h6 style="color:#b82171"><b>₱1,700</b></h6>
	</h4>

	<div class="row">
		<div class="col-12 col-md-4 mt-4">
			<img data-enlargable class="w-100" style="cursor: zoom-in"  src="{{asset('images/flowers/spring/rhapsody.jpeg')}}"/ height="400px">
			<hr>
		</div><!-- end of col -->

		<div class="col-12 col-md-4 mt-4">
			<h6 class="text-secondary">
				Let’s paint the town red! Shower your loved one with these all-red flowers to convey your feelings for her. This Rhapsody Red spring bouquet is sure to send your message of love and passion.
				<br><br>
				Every flower is meticulously hand-tied together, packaged with a classy water-proof wrapper and a matching ribbon. Each stem is kept fresh with water stored in a floral tube or floral foam. It comes with a greeting card where you can put into words how you feel.

				@include('sections.delivery')
				<br>
				@include('sections.novase3')
				<br><br>
				@include('sections.order')
			</h6>
		</div><!-- end of col -->

	</div><!-- end of row -->

</div><!-- End of card-->

<script>
$('img[data-enlargable]').addClass('img-enlargable').click(function(){
    var src = $(this).attr('src');
    $('<div>').css({
        background: 'RGBA(0,0,0,.5) url('+src+') no-repeat center',
        backgroundSize: 'contain',
        width:'100%', height:'100%',
        position:'fixed',
        zIndex:'10000',
        top:'0', left:'0',
        cursor: 'zoom-out'
    }).click(function(){
        $(this).remove();
    }).appendTo('body');
});
</script>
@endsection

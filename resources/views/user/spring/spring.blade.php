@extends('layouts.user')
@section('title', 'Sheilablossoms Spring Bouquets')
@section('content')

<div class="mt-2">
	<small>
		<a href="{{url('/')}}" class="text-secondary">Home</a> /&nbsp;
		<a href="{{url('/')}}" class="text-secondary">Flowers</a> /&nbsp;
		Spring Bouquets
	</small>
</div>


<div class="row">
	<div class="col-6 col-md-3 mt-5">
		<a style="color:#b82171" href="{{ url('/spring/carnival')}}">
			<img src="{{asset('images/flowers/spring/carnival.jpeg')}}" class="w-100" height="320px">
			<center>
				<h5 class="mt-3">Spring Carnival</h5>
		</a>
				<h6 class="text-secondary">₱1,800</h6>
				<h6 class="text-secondary">(Lisianthus, 4 Carnations, 1 Star Gazer)</h6>

			</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<a style="color:#b82171" href="{{ url('/spring/medley')}}">
			<img src="{{asset('images/flowers/spring/medley.jpeg')}}" class="w-100" height="320px">
			<center>
				<h5 class="mt-3">Spring Medley</h5>
		</a>
				<h6 class="text-secondary">₱2,200</h6>
				<h6 class="text-secondary">(Lisianthus, 4 Carnations, 1 Star Gazer, 3 Gerberas)</h6>
			</center>

	</div>

	<div class="col-6 col-md-3 mt-5">
		<a style="color:#b82171" href="{{ url('/spring/celebration')}}">
			<img src="{{asset('images/flowers/spring/celebration.jpeg')}}" class="w-100" height="320px">
			<center>
				<h5 class="mt-3">Spring Celebration</h5>
		</a>
			<h6 class="text-secondary">₱1,800</h6>
				<h6 class="text-secondary">(3 Carnations, 3 Gerberas, 3 Tulips)</h6>
			</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<a style="color:#b82171" href="{{ url('/spring/rhapsody')}}">
			<img src="{{asset('images/flowers/spring/rhapsody.jpeg')}}" class="w-100" height="320px">
			<center>
				<h5 class="mt-3">Rhapsody Red</h5>
		</a>
				<h6 class="text-secondary">₱1,700</h6>
				<h6 class="text-secondary">(3 Imported Red Roses, 3 Red Carnations, 3 Red Gerberas)</h6>
			</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<a style="color:#b82171" href="{{ url('/spring/pretty')}}">
			<img src="{{asset('images/flowers/spring/pretty.jpeg')}}" class="w-100" height="320px">
			<center>
				<h5 class="mt-3">Pretty in Pink</h5>
		</a>
				<h6 class="text-secondary">₱1,700</h6>
				<h6 class="text-secondary">(3 Imported Pink Roses, 3 pink Gerberas, 3 Pink Carnations)</h6>
			</center>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<a style="color:#b82171" href="{{ url('/spring/daffodil')}}">
			<img src="{{asset('images/flowers/spring/daffodil.jpeg')}}" class="w-100" height="320px">
			<center>
				<h5 class="mt-3">Daffodil Yellow</h5>
		</a>
				<h6 class="text-secondary">₱1,700</h6>
				<h6 class="text-secondary">(3 imported Yellow Roses, 3 Yellow Carnations, 3 Yellow Gerberas)</h6>
			</center>
	</div>

	<div class="col-12 col-md-4 mt-5 text-secondary">
		Bring a burst of spring to your special someone with this colorful selection of fresh flowers. Each flower is carefully hand-tied
		together by our well-trained florists to create a dazzling bouquet that is truly worthy of her.
		<br><br>
		@include('sections.novase2')
	</div>

</div><!-- end of row -->
@endsection
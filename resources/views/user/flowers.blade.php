@extends('layouts.user')
@section('title', 'Sheilablossoms Roses')
@section('content')
<div class="row">
	<div class="col-6 col-md-3 mt-5">
		<a href="{{ url('/roses/romance')}}" class="text-secondary">
			<img src="{{asset('images/flowers/roses/3.png')}}" class="w-100" height="320px">
			<center>
			<h4 class="text-danger mt-3">Romance</h4>
				<h6>3-stem Roses</h6>
			</center>
		</a>
	</div>

	<div class="col-6 col-md-3 mt-5">
		<a href="" class="text-secondary">
			<img src="{{asset('images/flowers/roses/6.jpg')}}" class="w-100" height="320px">
			<center>
				<h4 class="text-danger mt-3">Passion</h4>
				<h6>6-stem Roses</h6>
			</center>
		</a>
		
	</div>	

	<div class="col-6 col-md-3 mt-5">
		<a href="" class="text-secondary">
			<img src="{{asset('images/flowers/roses/12.jpg')}}" class="w-100" height="320px">
			<center>			
				<h4 class="text-danger mt-3">Luxe</h4>
				<h6>12-stem Roses</h6>
			</center>
		</a>
	</div>	

	<div class="col-6 col-md-3 mt-5">
		<a href="" class="text-secondary">
			<img src="{{asset('images/flowers/roses/24.jpg')}}" class="w-100" height="320px">
			<center>
				<h4 class="text-danger mt-3">Supreme</h4>
				<h6>24-stem Roses</h6>
			</center>
		</a>
	</div>	

</div><!-- end of row -->
@endsection
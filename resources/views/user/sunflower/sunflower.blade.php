@extends('layouts.user')
@section('title', 'Sheilablossoms Sunflower')
@section('content')
<div class="mt-2">
	<small>
		<a href="{{url('/')}}" class="text-secondary">Home</a> /&nbsp;
		<a href="{{url('/')}}" class="text-secondary">Flowers</a> /&nbsp;
		Sunflower
	</small>
</div>

<br>
<div class="row">
	<div class="col-6 col-md-2 mb-3">
		<center>
			<div style="color:#b82171" href="{{ url('/sunflower/3')}}">
			<img data-enlargable style="cursor: zoom-in" src="{{asset('images/flowers/sunflower/sf3.jpeg')}}" class="w-100" height="250px">

				<h5 class="mt-3">3-Stem Alegra Sunflowers</h5>
				<h6 class="text-secondary">₱1,050</h6>
			</div>
			<font size="2">Click here for the<a class="text-secondary" href="#" data-toggle="modal" data-target="#sun3"> <b>PACKAGING</b>.</a></font>
		</center>
	</div>


	<div class="col-6 col-md-2 mb-3">
		<center>
			<div style="color:#b82171" href="{{ url('/sunflower/6')}}">
			<img data-enlargable style="cursor: zoom-in" src="{{asset('images/flowers/sunflower/sf6.jpeg')}}" class="w-100" height="250px">

				<h5 class="mt-3">6-Stem Alegra Sunflowers</h5>
				<h6 class="text-secondary">₱1,800</h6>
			</div>
			<font size="2">Click here for the<a class="text-secondary" href="#" data-toggle="modal" data-target="#sun6"> <b>PACKAGING</b>.</a></font>
		</center>
	</div>


	<div class="col-6 col-md-2 mb-3">
		<center>
			<div style="color:#b82171" href="{{ url('/sunflower/12')}}">
			<img data-enlargable style="cursor: zoom-in" src="{{asset('images/flowers/sunflower/sf12.jpeg')}}" class="w-100" height="250px">
				<h5 class="mt-3">12-Stem Alegra Sunflowers</h5>
				<h6 class="text-secondary">₱3,600</h6>
			</div>
			<font size="2">Click here for the<a class="text-secondary" href="#" data-toggle="modal" data-target="#sun12"> <b>PACKAGING</b>.</a></font>
		</center>
	</div>

	<div class="col-12 col-md-6 mb-3">
		<h6 class="text-secondary mb-2">
			Alegra sunflowers never fail to cheer up someone. They are known to be happy flowers, making them a perfect gift to bring joy and brighten one’s day. This sun-like flower symbolizes adoration, loyalty and longevity.
			<br><br>
			See her glow when you send her these gorgeous flowers! Choose from three, six to twelve stem tulips to send to your special someone.
			<br><br>
			Every arm bouquet is packaged in a classy water- resistant wrapper with a matching ribbon. Each stem is kept fresh with water stored in a floral tube or floral foam. It also comes with a greeting card where you can put into words how you feel.
						@include('sections.delivery')
			<br>
			@include('sections.novase')
			<br><br>
			@include('sections.order')
		</h6>

	</div>
</div><!-- end of row -->

<!-- Modal -->
<div class="modal fade" id="sun3" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title" id="exampleModal3Label">Sample of Packaging</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>

      <div class="modal-body">
		<img src="{{asset('/images/flowers/sun3.jpg')}}" class="img-fluid" />
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div><!-- end of modal -->

<!-- Modal -->
<div class="modal fade" id="sun6" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title" id="exampleModal3Label">Sample of Packaging</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>

      <div class="modal-body">
		<img src="{{asset('/images/flowers/sun6.jpg')}}" class="img-fluid" />
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div><!-- end of modal -->

<!-- Modal -->
<div class="modal fade" id="sun12" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title" id="exampleModal3Label">Sample of Packaging</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>

      <div class="modal-body">
		<img src="{{asset('/images/flowers/sun12.jpg')}}" class="img-fluid" />
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div><!-- end of modal -->

<script>
$('img[data-enlargable]').addClass('img-enlargable').click(function(){
    var src = $(this).attr('src');
    $('<div>').css({
        background: 'RGBA(0,0,0,.5) url('+src+') no-repeat center',
        backgroundSize: 'contain',
        width:'100%', height:'100%',
        position:'fixed',
        zIndex:'10000',
        top:'0', left:'0',
        cursor: 'zoom-out'
    }).click(function(){
        $(this).remove();
    }).appendTo('body');
});


</script>

@endsection
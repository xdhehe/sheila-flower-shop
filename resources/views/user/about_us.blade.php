@extends('layouts.user')
@section('title', 'Sheilablossoms About Us')
@section('content')
<div class="row">
    <div class="col-12 mt-5">
		<div class="card">
		  <div class="card-body">
		    <h2 class="card-text text-center mb-2">Our Story</h2>
		    	<h5 class="text-secondary">
				  Sheila May’s Blossoms Flower and Gift Shop traces its roots back to 1996, when it first opened as Fresh Blossoms Flower and Gift Shop.<br><br>

				  Within its first few months of operations, it was immediately recognized for raising the quality and value of floral and aesthetic arrangements for all occasions not only in Los Banos, Laguna, but in adjoining municipalities and provinces as well.<br><br>

				  Twenty-two years hence, Sheila May’s Blossoms Flowers and Gift Shop has continued to advance the art and science of floral design, through continuous learning for its designers and staff on modern techniques and tools for floral arrangements, while ensuring the availability of highest quality and wide-ranging variety of flowers sourced through its broad network of local, national and global providers.<br><br>

				  Today, Sheila May’s Blossoms Flowers and Gift Shops is proud to have not only individual clients, but private companies, government organizations, and international organizations, in Laguna, Metro Manila, and neighboring provinces, among its loyal clients.  And we look forward to continuing to provide tailored services according to your needs!
			  	</h5>		    
		  </div>
		</div>
    </div>
</div>
@endsection
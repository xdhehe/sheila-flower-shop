@extends('layouts.user')
@section('title', 'Sheilablossoms Contact Us')

@section('content')
<div class="row text-secondary">


	<div class="col-12 col-md-6 mt-4">	
		<center>
			<h3><i class="fas fa-location-arrow"></i> Walk in on us</h3>
			<hr>
			<img src="/images/contact us/map.jpg" width="95%" height="400px">
		</center>
	</div>	

	<div class="col-12 col-md-6 mt-5">	
		<br>
		<h4><b>
		  	<i class="fas fa-map-marker-alt"></i> 10405 Lopez Avenue, Barangay Batong Malake, Los Baños, Laguna, Philippines 4030<br><br>
			<i class="fas fa-phone"></i> <a href="tel:(049) 536-5553" class="text-secondary">(+63) 49 536 5553</a> (PLDT)<br>
		  	<i class="fas fa-mobile-alt mr-2"></i><a href="tel:+639276333833" class="text-secondary">(+63) 927 633 3833 (Globe)</a><br>
		  	<i class="fas fa-mobile-alt mr-2"></i><a href="tel:+639477855534" class="text-secondary">(+63) 947 785 5534 (Smart)</a><br>
			<i class="far fa-envelope"></i><a href="mailto:sheilamaysblossoms@icloud.com?subject=From Sheila May's Flower Shop Website" target="_top" class="text-secondary"> sheilamaysblossoms@icloud.com</a><br><br>
			Follow Us<br>

			<h2>&nbsp;
				<a href="{{url('https://www.facebook.com/FreshBlossomsLaguna')}}"  target="_blank"><i class="fab fa-facebook-square text-secondary"></i></a>
			</h2>
		</b></h4>
	</div>

</div>
@endsection
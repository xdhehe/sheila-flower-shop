<style>
.demo {
    width:auto;
}
.demo ul {
    list-style: none outside none;
    padding-left: 0;
    margin-bottom:0;
}
.demo li {
    display: block;
    float: left;
    margin-right: 3px;
    cursor:pointer;
}

.demo img {
    display: block;
    height: auto;
    width: 100%;
} 
</style>

@extends('layouts.user')
@section('head')
@parent
<link href="{{asset('/css/lightslider.css')}}" rel="stylesheet" type="text/css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="{{asset('/js/lightslider.js')}}"></script>
@endsection

@section('title', 'Sheila FlowerShop Service Inquire')
@section('content')

<div class="card p-2 pt-3 mt-3">
  <center><h2 class="mb-2 m-2">{{$service->title}}</h2></center>
  <hr><br>

@if(count($photos) > 0)
  <div class="demo">
    <ul id="lightSlider">
    @foreach($photos as $photo)
    <?php $path = "images/services/$service->id/$photo->name"?>
      <li data-thumb="{{asset($path)}}">
        <img src="{{asset($path)}}" />
      </li>
    @endforeach
  </ul>
</div>

@endif


  <h5 class="m-2 mt-4">{{$service->description}}</h5><hr>
  <center>
    <button class="btn btn-success mt-3">
      <h5 class="p-2"><i class="far fa-edit"></i> Inquire Now</h5>
    </button>
    <a href="{{url('/contact_us')}}" target="_blank">
      <button class="btn btn-info text-white mt-3">
          <h5 class="p-2"><i class="fas fa-info"></i> Questions? Contact Us</h5>
      </button>
    </a>
  </center><br>
</div><!-- end of card -->


<script>
$('#lightSlider').lightSlider({
    gallery: true,
    item: 1,
    loop: true,
    slideMargin: 6,
    thumbItem: 3,

    speed: 400, //ms'
    auto: true,
    slideEndAnimation: true,
    pause: 2000,    
});
</script>

@endsection





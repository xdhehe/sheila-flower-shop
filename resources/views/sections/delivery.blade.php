<br><br>
<div class="text-secondary">
	We can deliver your flowers using our specially designed box. Flowers are exclusively transported by Sheila May’s Blossoms vehicle and personnel. Please click this <a class="text-secondary" href="#" data-toggle="modal" data-target="#idDeliveryFee"><b>DELIVERY FEES</b></a> link to check on the locale and its respective delivery fees. You may also opt to pick-up the flowers from our flower shop for a more personal touch to present it to someone.
</div>
<br>
@include('sections.addons')
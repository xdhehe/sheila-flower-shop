<nav class="navbar navbar-expand-md navbar-dark">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample08" aria-controls="navbarsExample08" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <a class="navbar-brand head ml-5" href="{{ url('/') }}">
      <img src="{{asset('/images/general/logo.png')}}" width="100px">
  </a>

  <div class="collapse navbar-collapse justify-content-md-center" id="navbarsExample08">
    <ul class="navbar-nav mr-auto">
        <!-- HOME -->
        <a class="text-white mr-3 mb-2 mt-1" href="{{ url('/')}}"><img src="{{asset('/images/icons/home.png')}}" class="icon"> <b>Home</b></a>

        <!-- ABOUT US -->
        <a class="text-white mr-3 mb-2 mt-1" href="{{ url('/about_us')}}"><img src="{{asset('/images/icons/about_us.png')}}" class="icon"> <b>Our Story</b></a>

        <!-- FLOWERS -->
        <div class="dropdown mr-3 mb-2 mt-1">
          <a class="dropdown-toggle text-white" href="{{ url('/services')}}">
            <img src="{{asset('/images/icons/flower.png')}}" class="icon"> <b>Flowers</b>
          </a>

          <!-- SERRVICES DROPDOWN -->
          <div class="dropdown-menu">
            <a href="{{ url('/valentines')}}" class="dropdown-item">Valentine’s Specials</a>
            <a href="{{ url('/roses')}}" class="dropdown-item">Roses</a>
            <a href="{{ url('/tulips')}}" class="dropdown-item">Tulips</a>
            <a href="{{ url('/sunflower')}}" class="dropdown-item">Sunflowers</a>
            <a href="{{ url('/mixed')}}" class="dropdown-item">Mixed Bouquets</a>
            <a href="{{ url('/spring')}}" class="dropdown-item">Spring Bouquets</a>
          </div>
        </div>

        <!-- SERVICES-->
        <div class="dropdown mr-3 mb-2 mt-1">
          <a class="dropdown-toggle text-white" href="{{ url('/services')}}">
            <img src="{{asset('/images/icons/service.png')}}" class="icon"> <b>Services</b></a>
          </a>

          <!-- SERRVICES DROPDOWN -->
          <div class="dropdown-menu">
            @if(count($globalServices)>0)
              @foreach($globalServices as $globalService)
              <?php $path = "/services/$globalService->title"?>
                <a href="{{url($path)}}" class="dropdown-item">{{$globalService->title}}</a>
              @endforeach
            @endif
                <a href="#" class="dropdown-item">Others</a>
          </div>
        </div>

        <!-- CONTACT US -->
        <a class="text-white mr-3 mb-2 mt-1" href="{{ url('/contact_us')}}"><img src="{{asset('/images/icons/contact_us.png')}}" class="icon"> <b>Contact Us</b></a>

        <!-- CONTACT US -->
        <a class="text-white mr-3 mb-2 mt-1" href="{{ url('/faq')}}"><img src="{{asset('/images/icons/faq.png')}}" class="icon"> <b>FAQ</b></a>
  </ul>
  </div><!-- END OF COLLAPSABLE -->
</nav><!-- END OF WHOLE NAV -->


<footer style="background: rgba(180, 46, 117, 0.6);" class="text-white mt-5">
<div class="container">
	<div class="row">
		<div class="col-12 col-md-8">
			<b>
				<a class="text-white" href="#" data-toggle="modal" data-target="#idDeliveryFee">
					Delivery Fees
				</a><br>
				<!-- Terms & Conditions<br> -->

				<a href="{{ url('/contact_us')}}" class="text-white"><b>Contact Us</b></a><br>
			</b>
			  	&emsp;<i class="fas fa-map-marker-alt"></i> 10405 Lopez Avenue, Barangay Batong Malake, Los Baños, Laguna, Philippines 4030<br>
				&emsp;<i class="fas fa-phone"></i> <a href="tel:(049) 536-5553" class="text-white">(+63) 49 536 5553</a> (PLDT)<br>
			  	&emsp;<i class="fas fa-mobile-alt mr-2"></i><a href="tel:+639276333833" class="text-white">(+63) 927 633 3833 (Globe)</a><br>
			  	&emsp;<i class="fas fa-mobile-alt mr-2"></i><a href="tel:+639477855534" class="text-white">(+63) 947 785 5534 (Smart)</a><br>
				&emsp;<i class="far fa-envelope"></i><a href="mailto:sheilamaysblossoms@icloud.com?subject=From Sheila May's Flower Shop Website" target="_top" class="text-white"> sheilamaysblossoms@icloud.com</a><br>
		</div>


		<div class="col-12 col-md-4">
			<b>Payment Options</b>
			<h2>&nbsp;
				<img width="90px" src="{{asset('images/icons/palawan.png')}}">
				<img width="40px" src="{{asset('images/icons/lbc.png')}}">
				<img width="60px" src="{{asset('images/icons/bpi.png')}}">
				<img width="90px" src="{{asset('images/icons/wu.png')}}">
				<!-- <img width="55px" src="{{asset('images/icons/cl.png')}}"> -->
				<br>
			</h2>

				<b>Follow Us</b><br>

				<h2>&nbsp;
					<a href="{{url('https://www.facebook.com/FreshBlossomsLaguna')}}"  target="_blank"><i class="fab fa-facebook-square text-white"></i></a>
			  		<!-- <a href="{{url('https://www.instagram.com/sheilablossoms')}}"      target="_blank"><i class="fab fa-instagram text-white"></i></a> -->
				</h2>
		</div>
	</div>
	<p><b>Copyright © 2018 sheilablossoms.com, All Rights Reserved.</b></p>

</div>
</footer>

<!-- Modal -->
<div class="modal fade" id="idDeliveryFee" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModal3Label">Fees for Laguna Deliveries
        	<br/>
        	<span style="font-size: 15px;">Please note there is a <b>minimum value order</b> for delivery.</span>
    	</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
		<table class="table table-striped text-secondary">
			<tr>
				<td><b>Location</b></td>
				<td><b>&nbsp;Minimum Value Order</b></td>
				<td><b>&nbsp;Delivery Charges</b></td>
			</tr>

			<tr>
				<td><li>Bay</li></td>
				<td>&nbsp;₱1,500</td>
				<td>&nbsp;₱400</td>
			</tr>

			<tr>
				<td><li>Cabuyao</li></td>
				<td>&nbsp;₱2,500</td>
				<td>&nbsp;₱700</td>
			</tr>

			<tr>
				<td><li>Calamba</li></td>
				<td>&nbsp;₱2,500</td>
				<td>&nbsp;₱600</td>
			</tr>

			<tr>
				<td><li>Calauan</li></td>
				<td>&nbsp;₱2,000</td>
				<td>&nbsp;₱500</td>
			</tr>

			<tr>
				<td><li>Liliw</li></td>
				<td>&nbsp;₱2,500</td>
				<td>&nbsp;₱700</td>
			</tr>

			<tr>
				<td><li>Los Banos</li></td>
				<td>&nbsp;₱1,300</td>
				<td>&nbsp;₱200</td>
			</tr>

			<tr>
				<td><li>Pagsanjan</li></td>
				<td>&nbsp;₱2,500</td>
				<td>&nbsp;₱800</td>
			</tr>

			<tr>
				<td><li>Pila</li></td>
				<td>&nbsp;₱2,000</td>
				<td>&nbsp;₱600</td>
			</tr>

			<tr>
				<td><li>San Pablo</li></td>
				<td>&nbsp;₱2,000</td>
				<td>&nbsp;₱700</td>
			</tr>

			<tr>
				<td><li>Santa Cruz</li></td>
				<td>&nbsp;₱2,500</td>
				<td>&nbsp;₱700</td>
			</tr>

			<tr>
				<td><li>Victoria</li></td>
				<td>&nbsp;₱1,700</td>
				<td>&nbsp;₱500</td>
			</tr>
		</table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div><!-- end of modal -->


<!-- Modal -->
<div class="modal fade" id="idNoVase2" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title" id="exampleModal3Label">Sample of Packaging</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>

      <div class="modal-body">
      	@if(isset($style) && isset($color))
      		<!-- for roses -->
	      	@if($style == "romance")
	      		@if($color == "pink")
      				<img src="{{asset('/images/flowers/rom-pink.jpg')}}" class="img-fluid" />
  				@else
					<img src="{{asset('/images/flowers/rom-yellow.jpg')}}" class="img-fluid">
      			@endif
      		@endif

	      	@if($style == "passion")
	      		@if($color == "")
					<img src="{{asset('/images/flowers/pas-yellow.jpg')}}" class="img-fluid w-100">
				@endif
      		@endif

	      	@if($style == "luxe")
	      		@if($color == "pink")
      				<img src="{{asset('/images/flowers/lux-pink.jpg')}}" class="img-fluid" />
  				@elseif($color == "yellow")
  					<img src="{{asset('/images/flowers/lux-yellow.jpg')}}" class="img-fluid" />
  				@else
					<img src="{{asset('/images/flowers/lux-red.jpg')}}" class="img-fluid">
      			@endif
      		@endif

	      	@if($style == "supreme")
	      		@if($color == "")
					<img src="{{asset('/images/flowers/sup-red.jpg')}}" class="img-fluid">
      			@endif
      		@endif

		@endisset
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div><!-- end of modal -->

<!-- Modal -->
<div class="modal fade" id="idNoVase3" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title" id="exampleModal3Label">Sample of Packaging</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>

      <div class="modal-body">
      	<img src="{{asset('/images/flowers/packaged3.jpg')}}" class="img-fluid">
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div><!-- end of modal -->

<!-- Modal -->
<div class="modal fade" id="idAddons" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="exampleModal3Label">Add-ons</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>

      <div class="modal-body">
		<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
		  <ol class="carousel-indicators">
		    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
		    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
		    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
		    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
		    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
		  </ol>

		  <div class="carousel-inner" role="listbox">
		    <div class="carousel-item active">
		      <img class="d-block w-100" src="{{asset('/images/addons/ferrero1.jpeg')}}">
		      <div class="carousel-caption d-none d-md-block">
	            <h4 class="text-dark">₱350</h4>
        	  </div>
		    </div>
		    <div class="carousel-item">
		      <img class="d-block w-100" src="{{asset('/images/addons/ferrero11.jpeg')}}">
		      <div class="carousel-caption d-none d-md-block">
	            <h4 class="text-dark">₱550</h4>
        	  </div>
		    </div>
		    <div class="carousel-item">
		      <img class="d-block w-100" src="{{asset('/images/addons/ferrero2.jpeg')}}">
		      <div class="carousel-caption d-none d-md-block">
	            <h4 class="text-dark"><b>₱400</b></h4>
        	  </div>
		    </div>

		    <div class="carousel-item">
		      <img class="d-block w-100" src="{{asset('/images/addons/ferrero3.jpeg')}}">
		      <div class="carousel-caption d-none d-md-block">
	            <h4 class="text-dark"><b>₱750</b></h4>
        	  </div>
		    </div>

		    <div class="carousel-item">
		      <img class="d-block w-100" src="{{asset('/images/addons/bear.jpeg')}}">
		      <div class="carousel-caption d-none d-md-block">
	            <h4 class="text-dark"><b>₱750</b></h4>
        	  </div>
		    </div>

		    <div class="carousel-item">
		      <img class="d-block w-100" src="{{asset('/images/addons/bear_hug.jpeg')}}">
		      <div class="carousel-caption d-none d-md-block">
	            <h4 class="text-dark"><b>₱1,050</b></h4>
        	  </div>
		    </div>
		  </div>

		  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
		    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
		    <span class="sr-only">Previous</span>
		  </a>
		  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
		    <span class="carousel-control-next-icon" aria-hidden="true"></span>
		    <span class="sr-only">Next</span>
		  </a>
		</div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div><!-- end of modal -->
	<div class="row">
		<div class="col-md-1 col-sm-3 mt-1">
			<h4 class="mt-3">Category: </h4>
		</div>
		<div class="col-md-3 col-sm-8 mt-3">
		{!! Form::open(['action' => 'FlowersController@sort','method' => 'POST']) !!}
		@csrf				
			<div class="input-group">
				<select class="custom-select" name="category" required>
				  <option value="All" selected>All</option>

				  @if(count($categories) > 0)
				  	@foreach($categories as $category)
				  	<option value="{{$category->name}}">{{$category->name}}</option>
				  	@endforeach
				  @endif
				  
				</select>
				<div class="input-group-append">
					<button class="btn btn-success">Go</button>
				</div>
			</div>
		{!! Form::close() !!}
	    </div>



		<div class="col-md-3 col-sm-11 mt-3">
		{!! Form::open(['action' => 'FlowersController@search','method' => 'POST']) !!}
		@csrf
			<div class="input-group">
			    <input type="text" class="form-control" placeholder="Search" name="search" required>
			    <div class="input-group-append">
			      <button class="btn btn-success"><i class="fa fa-search"></i></button>
			    </div>
			</div>        
		{!! Form::close() !!}			
		</div>	    
 	</div><!-- END OF ROW -->
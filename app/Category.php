<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

  	public function flowers()
    {
        return $this->hasMany('App\Flower')->orderBy('order','asc');
    }


	public function delete()
    {
        // delete all related photos 
        $this->flowers()->delete();
        // delete the user
        return parent::delete();
    }    	
}

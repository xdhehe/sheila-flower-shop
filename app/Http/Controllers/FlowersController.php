<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Flower;
use App\Category;
use File;

class FlowersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {   
        $category = Category::find($id);
        $flowers = Category::find($id)->flowers;

        return view('auth.category.flower_index')->with('flowers',$flowers)
                                                 ->with('category',$category);
    }
    /**
     * Show the `m for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($categoryId)
    {   
        //lagay yung last value
        $count = Category::find($categoryId)->flowers->count();
        $count++;

        $localCategory  = Category::find($categoryId);

        return view('auth.category.flower_create')->with('categoryId',$categoryId)
                                                  ->with('count',$count)
                                                  ->with('localCategory',$localCategory);        
    }


    public function store(Request $request, $categoryId)
    {
        $this->validate($request, [
        'category' => 'required',
        'order' => 'required',
        'name' => 'required',
        'image'  => 'required|image|mimes:jpg,png,jpeg|max:2048',
        ]);

        $category = $request->input('category');

        $image = $request->file('image');
        $new_name = rand() . '.' . $image->getClientOriginalExtension();
        $image->move(public_path("images/flowers/$category"), $new_name);

        $flower = new Flower;

        $flower->category_id = $request->input('category');
        $flower->order = $request->input('order');
        $flower->name = $request->input('name');
        $flower->description = $request->input('description');
        $flower->price = $request->input('price');
        $flower->picture = $new_name;

        $flower->save();

        $path = "/dashboard/categories/$category/flowers";

        return redirect($path)->with('success', "Successfully Created a Flower");
    }

    public function show($id){

    }


    public function updates(Request $request)
    {
        $id = $request->input('id');

        $this->validate($request, [
        'order' => 'required',
        'name' => 'required',
        ]);

        $category = $request->input('category');

        $flower = Flower::find($id);

        $flower->order = $request->input('order');
        $flower->name = $request->input('name');
        $flower->description = $request->input('description');
        $flower->price = $request->input('price');

        $flower->save();

        $path = "/dashboard/categories/$category/flowers";

        return redirect($path)->with('success', "Successfully Updated");
    }

    public function destroys(Request $request)
    {
        $category = $request->input('category');
        $picture = $request->input('picture');
        File::delete(public_path("images/flowers/$category/$picture"));
        
        $id = $request->input('id');
        $photo = Flower::find($id);
        $photo->delete();

        $path = "/dashboard/categories/$category/flowers";
        return redirect($path)->with('success', "Flower Deleted Successfully");
    }

    // public function photo($id){
    //     $flower = Flower::find($id);
    //     return view('auth.flower.photo_index')->with('flower',$flower);
    // }

    // public function photo_store(Request $request){
    //     $id = $request->input('id');
    //     $category = $request->input('category');

    //     $this->validate($request, [
    //     'image'  => 'required|image|mimes:jpg,png,jpeg|max:2048',
    //     ]);
    //     $image = $request->file('image');
    //     $new_name = rand() . '.' . $image->getClientOriginalExtension();
    //     $image->move(public_path("images/flowers/$category"), $new_name);

    //     $flower = Flower::find($id);
    //     $flower->picture = $new_name;
    //     $flower->save();

    //     return redirect('/dashboard/flowers')->with('success','Added a photo successfully');
    // }

    // public function photo_delete(Request $request){
    //     $id       = $request->input('id');
    //     $category = $request->input('category');
    //     $picture  = $request->input('picture');

    //     $flower = Flower::find($id);
    //     $flower->picture = "null";
    //     $flower->save();

    //     File::delete(public_path("images/flowers/$category/$picture"));

    //     return redirect('/dashboard/flowers')->with('success','Deleted the photo Successfully');
    // }

    // public function sort(Request $request)
    // {
    //     $input = $request->input('category');
        
    //     if($input == "All")
    //         $flowers = Flower::all();
    //     else
    //         $flowers = Flower::where('category',$input)->get();

    //     return view('auth.flower.index')->with('flowers',$flowers);
    // }

    // public function search(Request $request){
    //     $input = $request->input('search');

    //     if($input == "")
    //         $flowers = Flower::all();
    //     else
    //         $flowers = Flower::where('title','like',"$input%")->get();
        
    //     return view('auth.flower.index')->with('flowers', $flowers);
    // }

}
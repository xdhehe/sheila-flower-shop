<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use File;

class CategoriesController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $categories = Category::orderBy('name','asc')->get();
        return view('auth.category.index')->with('categories',$categories);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('auth.category.create');        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
        ]);

        $category = new Category;
        $category->name = $request->input('name');
        $category->save();

        return redirect('/dashboard/categories')->with('success','Category Created');       

    }

    public function edit(){
        return view('auth.category.edit');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
        'name' => 'required',
        ]);

        $categoryName = Category::find($id);
        $categoryName->name = $request->input('name');
        $categoryName->save();

        $path = "/dashboard/categories";
        return redirect($path)->with('success', "Successfully Updated Category Name");        
    }    



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
    // deleting the row and related rows
        $category->delete();
    // Deleting the whole folder
        File::deleteDirectory(public_path("images/flowers/$id"));

        return redirect('/dashboard/categories')->with('success','Category Removed');
    }
}

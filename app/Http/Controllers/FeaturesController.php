<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\feature;
use File;

class FeaturesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($category){
        $photos = feature::orderBy('order','asc')->where('category',$category)->get();
        return view('auth.features.index')->with('photos',$photos)
                                          ->with('category',$category);
    }

    public function create($category){
        $photos = feature::where('category',$category)->count();
        $photos++;
        return view('auth.features.create')->with('photos',$photos)
                                           ->with('category',$category);;
    }


    public function store(Request $request){
        $this->validate($request, [
        'image'  => 'required|image|mimes:jpg,png,jpeg|max:2048',
        'order' => 'required',
        'category' => 'required',
        ]);
        $image = $request->file('image');
        $new_name = rand() . '.' . $image->getClientOriginalExtension();
        $image->move(public_path("images/features"), $new_name);


        $photo = new feature;
        $photo->name = $new_name;
        $photo->order = $request->input('order');
        $photo->category = $request->input('category');
        $photo->save();

        $category = $request->input('category');
        $path = "/dashboard/features/$category";

        return redirect($path)->with('success', "Added Photo Successfully in $category");
    }

    public function edit($category, $id)
    {
        $photo = feature::find($id);
        
        return view('auth.features.edit')->with('photo',$photo)
                                         ->with('category',$category)
                                         ->with('id',$id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function update(Request $request)
    {
        $this->validate($request, [
        'order' => 'required',
        ]);

        $id    = $request->input('id');
        $category = $request->input('category');

        $photo = feature::find($id);
        $photo->order = $request->input('order');
        $photo->save();

        $path = "/dashboard/features/$category";
        return redirect($path)->with('success', "Photo has been updated in $category");        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $name = $request->input('name');
        $id = $request->input('id');
        $category = $request->input('category');

        File::delete(public_path("images/features/$name"));
        $photo = feature::find($id);
        $photo->delete();

        $path = "/dashboard/features/$category";
        
        return redirect($path)->with('success',"Photo Removed in $category");;
    }    


}

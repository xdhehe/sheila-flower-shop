<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Flower;
use App\Service;
use App\feature;
use App\Category;
use Carbon\Carbon;

class PagesController extends Controller
{

	public function index(){
		$features = feature::where('category','featured')->orderBy('order','asc')->get();

		return view('user.home')->with('features',$features);
	}


	public function valentines(){
		return view('user.valentines.valentines');
	}
		public function vd1(){
			return view('user.valentines.vd1');
		}

		public function vd2(){
			return view('user.valentines.vd2');
		}

		public function vd3(){
			return view('user.valentines.vd3');
		}

		public function vd4(){
			return view('user.valentines.vd4');
		}




	public function roses(){
		return view('user.roses.roses');
	}

		public function romance(){
			return view('user.roses.romance.romance');
		}

			public function romanceRed(){
				return view('user.roses.romance.romance_red');
			}

			public function romancePink(){
				return view('user.roses.romance.romance_pink');
			}

			public function romanceYellow(){
				return view('user.roses.romance.romance_yellow');
			}

			public function romanceWhite(){
				return view('user.roses.romance.romance_white');
			}

			public function romancePeach(){
				return view('user.roses.romance.romance_peach');
			}

			public function romanceOrange(){
				return view('user.roses.romance.romance_orange');
			}

			public function romancePerwinkle(){
				return view('user.roses.romance.romance_perwinkle');
			}

			public function romanceSimone(){
				return view('user.roses.romance.romance_simone');
			}

		public function passion(){
			return view('user.roses.passion.passion');
		}

			public function passionRed(){
				return view('user.roses.passion.passion_red');
			}

			public function passionPink(){
				return view('user.roses.passion.passion_pink');
			}

			public function passionYellow(){
				return view('user.roses.passion.passion_yellow');
			}

			public function passionWhite(){
				return view('user.roses.passion.passion_white');
			}

			public function passionPeach(){
				return view('user.roses.passion.passion_peach');
			}

			public function passionOrange(){
				return view('user.roses.passion.passion_orange');
			}

			public function passionPerwinkle(){
				return view('user.roses.passion.passion_perwinkle');
			}

			public function passionSimone(){
				return view('user.roses.passion.passion_simone');
			}


		public function luxe(){
			return view('user.roses.luxe.luxe');
		}

			public function luxeRed(){
				return view('user.roses.luxe.luxe_red');
			}

			public function luxePink(){
				return view('user.roses.luxe.luxe_pink');
			}

			public function luxeYellow(){
				return view('user.roses.luxe.luxe_yellow');
			}

			public function luxeWhite(){
				return view('user.roses.luxe.luxe_white');
			}

			public function luxePeach(){
				return view('user.roses.luxe.luxe_peach');
			}

			public function luxeBlaze(){
				return view('user.roses.luxe.luxe_blaze');
			}

			public function luxeCoral(){
				return view('user.roses.luxe.luxe_coral');
			}

			public function luxeAmber(){
				return view('user.roses.luxe.luxe_amber');
			}

			public function luxeSparkle(){
				return view('user.roses.luxe.luxe_sparkle');
			}

			public function luxeTwilight(){
				return view('user.roses.luxe.luxe_twilight');
			}

			public function luxeTwinkle(){
				return view('user.roses.luxe.luxe_twinkle');
			}

			public function luxeShimmer(){
				return view('user.roses.luxe.luxe_shimmer');
			}



		public function supreme(){
			return view('user.roses.supreme.supreme');
		}

			public function supremeRed(){
				return view('user.roses.supreme.supreme_red');
			}

			public function supremePink(){
				return view('user.roses.supreme.supreme_pink');
			}

			public function supremeYellow(){
				return view('user.roses.supreme.supreme_yellow');
			}

			public function supremeWhite(){
				return view('user.roses.supreme.supreme_white');
			}


	public function tulips(){
		return view('user.tulips.tulips');
	}

	public function sunflower(){
		return view('user.sunflower.sunflower');
	}


	public function mixed(){
		return view('user.mixed.mixed');
	}

	public function spring(){
		return view('user.spring.spring');
	}

			public function springCarnival(){
				return view('user.spring.carnival');
			}
			public function springMedley(){
				return view('user.spring.medley');
			}

			public function springCelebration(){
				return view('user.spring.celebration');
			}

			public function springRhapsody(){
				return view('user.spring.rhapsody');
			}

			public function springPretty(){
				return view('user.spring.pretty');
			}

			public function springDaffodil(){
				return view('user.spring.daffodil');
			}


	public function mother(){
		return view('user.mother.mother');
	}
			public function basket(){
				return view('user.mother.basket');
			}
			public function pastel(){
				return view('user.mother.pastel');
			}

	public function circle(){
		return view('user.circle.circle');
	}

	public function complete(){
		return view('user.complete.complete');
	}







	public function flower(){
		return view('user.flowers');
	}



		public function deeper($category){
			$flowers = Category::find($category)->flowers()->paginate(6);

			return view('user.flower_deeper')->with('flowers',$flowers)
											 ->with('category',$category);
		}

			public function order($category, $id){
				$flower = Flower::where('id',$id)->first();

				return view('user.order')->with('category',$category)
										 ->with('flower',$flower);
			}


	public function service(){
		return view('user.service');
	}

		public function inquire($name){
			$service = Service::where('title',$name)->first();
			$photos = Service::find($service->id)->spictures;

			return view('user.inquire')->with('service',$service)
									    ->with('photos',$photos);
		}

			public function inquiry_form(){
				return view('user.inquiry_form');
			}


	public function contact(){
		return view('user.contact_us');
	}

	public function about(){
		$employees = feature::where('category','employee')->orderBy('order','asc')->get();
		return view('user.about_us')->with('employees',$employees);
	}


	public function faq(){
		return view('user.faq');
	}

	public function search(Request $request){
        $input = $request->input('search');

    	$services = Service::where('title','like',"$input%")->get();

        $flowers = Flower::where('name','like',"$input%")->get();

    	if($input!=""){
        	$view = view('user.search')->with('flowers', $flowers)
        		   					   ->with('services',$services)
        		   					   ->with('input',$input);
	  	}
	  	else
	  		$view = redirect('/');

        return $view;
	}

	public function show($id){
		$flower = Flower::find($id);
		return view('user.order')->with('flower',$flower);
	}


	// public function test(){
	// 	return view('user.test');
	// }
}
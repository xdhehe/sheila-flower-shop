<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use App\Spicture;
use File;

class ServicesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::all();
        return view('auth.service.index')->with('services',$services);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $last = Service::count();
        $last++;
        return view('auth.service.create')->with('last',$last);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'order' => 'required',
            'name' => 'required',
            'description' => 'required',
        ]);

        $service = new Service;
        $service->order = $request->input('order');
        $service->title = $request->input('name');
        $service->description = $request->input('description');
        $service->save();

        return redirect('/dashboard/services')->with('success','Service Created');        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = Service::find($id);
        return view('auth.service.edit')->with('service',$service);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
        'order' => 'required',
        'name' => 'required',
        'description' => 'required',
        ]);

        $photo = Service::find($id);
        $photo->order = $request->input('order');
        $photo->title = $request->input('name');
        $photo->description = $request->input('description');
        $photo->save();

        $path = "/dashboard/services/";
        return redirect($path)->with('success', "Service has been updated");        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service = Service::find($id);
//deleting the row and related to its
        $service->delete();
// Deleting the whole folder
        File::deleteDirectory(public_path("images/services/$id"));

        return redirect('/dashboard/services')->with('success','Service Removed');
    }


    public function photoIndex($id){
        $photos = Service::find($id)->spictures;
        $service = Service::find($id);

        return view('auth.service.photo_index')->with('id',$id)
                                                ->with('service',$service)
                                                ->with('photos',$photos);
    }

    public function photoAdd($id){
        $service = Service::find($id);        
        $photosCount = Service::find($id)->spictures->count();
        $photosCount++;

        return view('auth.service.photo_add')->with('service',$service)
                                             ->with('photosCount',$photosCount);
    }

    public function photoStore(Request $request){
        $service_id = $request->input('service_id');
        
        $this->validate($request, [
        'image'  => 'required|image|mimes:jpg,png,jpeg|max:2048',
        'order' => 'required',
        'service_id' => 'required',
        ]);

        $image = $request->file('image');
        $new_name = rand() . '.' . $image->getClientOriginalExtension();
        $image->move(public_path("images/services/$service_id"), $new_name);


        $photo = new Spicture;
        $photo->service_id = $service_id;
        $photo->name = $new_name;
        $photo->order = $request->input('order');
        $photo->save();

        $path = "/dashboard/services/photos/$service_id/index";
        return redirect($path)->with('success', "Added Photo Successfully");
    }

    public function photoEdit($id,$photoId){
        $service = Service::find($id);
        $spicture = SPicture::find($photoId);

        return view('auth.service.photo_edit')->with('service',$service)
                                              ->with('spicture',$spicture);
    }

    public function photoUpdate(Request $request){
        $this->validate($request, [
        'order' => 'required',
        ]);

        $id        = $request->input('id');
        $pictureId = $request->input('pictureId');

        $photo = Spicture::find($pictureId);
        $photo->order = $request->input('order');
        $photo->save();

        $path = "/dashboard/services/photos/$id/index";
        return redirect($path)->with('success', "Photo Order has been updated");        
    }

    public function photoDestroy(Request $request){
        $id = $request->input('id');
        $name = $request->input('name');
        $photoId = $request->input('photoId');

        File::delete(public_path("images/services/$id/$name"));
        $photo = Spicture::find($photoId);
        $photo->delete();
        $path = "/dashboard/services/photos/$id/index";
        return redirect($path)->with('success',"Photo Removed Successfully");
    }



}

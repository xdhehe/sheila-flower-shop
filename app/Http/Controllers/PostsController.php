<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PostsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $posts = Post::all();
        // $posts = Post::where('id','2')->get();
        // $posts = Post::orderBy('id','desc')->get();
        $posts = Post::orderBy('id','desc')->get();
        return view('auth.announcement.index')->with('posts',$posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('auth.announcement.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'date' => 'required',
            'description' => 'required',
        ]);

        $post = new Post;
        $post->description = $request->input('description');
        $post->date = $request->input('date');
        $post->save();

        return redirect('/dashboard/posts')->with('success','Post Created');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post= Post::find($id);
        return view('auth.announcement.edit')->with('post',$post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'date' => 'required',
            'description' => 'required',
        ]);

        $post = Post::find($id);
        $post->description = $request->input('description');
        $post->date = $request->input('date');
        $post->save();

        return redirect('/dashboard/posts')->with('success','Post Updated');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'date' => 'required',
            'description' => 'required',
        ]);

        $post = Post::find($id);
        $post->description = $request->input('description');
        $post->date = $request->input('date');
        $post->save();

        return redirect('/dashboard/posts')->with('success','Post Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $post ->delete();
        return redirect('/dashboard/posts')->with('success','Post Removed');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
  	public function spictures()
    {
        return $this->hasMany('App\Spicture')->orderBy('order','asc');
    }


	public function delete()
    {
        // delete all related photos 
        $this->spictures()->delete();
        // delete the user
        return parent::delete();
    }    
}
